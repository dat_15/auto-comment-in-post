<?php 

/**
* Author Nguyễn Tiến Đạt
*/
class DB {
    private $HOST_NAME = "112.78.2.163";
	private $USER_NAME = "tudo9772_admin";
	private $USER_PASS = "6489368436328";
	private $DB_NAME = "tudo9772_auto_comment";
	private $conn;
    private $fields     = '*';
    private $where      = '';
    private $orderby    = '';
    private $groupby    = '';
    private $limit      = '';
    private $table      = '';
    private $sql        = '';
	function __construct($table){
		$this->conn = new mysqli($this->HOST_NAME, $this->USER_NAME, $this->USER_PASS, $this->DB_NAME);
        if ($this->conn->connect_errno) {
            die ("Failed to connect to MySQL: (" . $this->conn->connect_errno . ") " . $this->conn->connect_error );
        }
        $this->table = $table;
        $this->VN_transfer();
    }

    public function connect_to($db_name){
        $new_conn = new mysqli($this->HOST_NAME, $this->USER_NAME, $this->USER_PASS, $db_name);
        if ($new_conn->connect_errno) {
            die ("Failed to connect to MySQL: (" . $new_conn->connect_errno . ") " . $new_conn->connect_error );
        }else{
            $this->conn = $new_conn;
        }
    }

    public function VN_transfer(){
    	$stmt = $this->conn->prepare('SET NAMES utf8');
    	$stmt->execute();

    	$stmt = $this->conn->prepare("set collation_connection='utf8_general_ci'");
    	$stmt->execute();

    }

    public function group_by($group_by = ''){
        if (!empty($group_by)) {
            $this->groupby = " GROUP BY $group_by ";
        }
        return $this;
    }

    public function order_by($order_by = ''){
        if (!empty($order_by)) {
            $orderby = $this->conn->real_escape_string ($orderby);
            $this->orderby = " ORDER BY $order_by ";
        }
        return $this;
    }

    public function limit($limit = PER_PAGE, $offset = 0){
        $limit = $this->conn->real_escape_string ($limit);
        $offset = $this->conn->real_escape_string ($offset);
        $this->limit = " LIMIT $limit OFFSET $offset;";
        return $this;
    }

    public function where($where){
        if(!empty($where)){
            $this->where = self::build_where($where);
        }
        return $this;
    }

    public function select($_fileds){
        $_fields = $this->conn->real_escape_string ($_fields);
        $this->fields = $_fields;
        return $this;
    }

    private function build_sql(){
        $this->sql = "SELECT ".$this->fields." FROM ".$this->table." " . $this->where . $this->orderby . $this->groupby . $this->limit;
    }

    public function get_sql(){
        return $this->sql;
    }

	/**
     * [get_table Show table's records]
     * @param  [string] $table_name [Table name]
     * @param  [int]    $limit      [Limit number of records received]
     * @return [array]  [An Array contains all record's data ]
     */
    public function get_table() {
        self::build_sql();
        $result = $this->query($this->sql);
        $data = array();

        // Kiểm tra có kết quả trả về hay không
        if ($result !== false && $result->num_rows > 0) {

            // Gán dữ liệu từ sql vào mảng trả về
            while($row = $result->fetch_assoc()) {
                $data[] = $row;
            }
        }

        return $result;
    }
    /**
     * [sql Handle an sql query]
     * @param  [string] $sql [sql query]
     * @return [array]      [trả về mảng dữ liệu]
     */
    public function query($_sql) {
        $result = $this->conn->query($_sql);
        $data = array();

        // Kiểm tra có kết quả trả về hay không
        if ($result !== false && $result->num_rows > 0) {
            // Gán dữ liệu từ sql vào mảng trả về
            while($row = $result->fetch_assoc()) {
                $data[] = $row;
            }
            return $data;
        }
        elseif($result !== false && $result->num_rows < 1){
            // Chỉ thực hiện lệnh bình thường
            return true;
        }
        // Lỗi
        return false;

    }

    /**
     * [add_row Thêm một bản ghi mới vào bảng]
     * @param [array] $data  [mảng dữ liệu đầu vào]
     * @param [string] $table [tên bảng để thêm dữ liệu]
     * @return [boolean/int] [trạng thái xử lý lệnh (false) hoặc ID bản ghi mới thêm]
     */
    public function add_row($data){
        $real_data = [];
        foreach ($data as $key => $value) {
            $real_data[$key] = $this->conn->real_escape_string ($value);
        }
        // Lấy tên các trường trong bảng
        $key = array_keys($data);
        $_sql = "INSERT INTO `" . $this->table . "`(" . "`" . implode("`,`",$key) . "`" . ") VALUES (" . "\"" . implode("\",\"",$real_data) . "\"" . ");";
        if ( $this->conn->query($_sql) )
            // Trả về ID bản ghi mới thêm
            return intval($this->conn->insert_id);
        else 
            // Lỗi insert
            return false;
    }


    /**
     * [get_row Lấy dữ liệu trên một bản ghi]
     * @param  [string] $table [Tên bảng]
     * @param  [array] $where [mảng điều kiện]
     * @return [array]        [mảng dữ liệu trả về nếu có hoặc một mảng rỗng]
     */
    public function get_row($table = null){
        if(!empty($table)) $this->table = $table;
        self::build_sql();
        $result = $this->conn->query($this->sql);
        // Kiểm tra dữ liệu trả về có hoặc không
        if ($result !== false && $result->num_rows > 0)
            return $result->fetch_assoc();
        else 
            return array();
    }


    /**
     * [get_rows Lấy dữ liệu trên nhiều bản ghi]
     * @param  [string] $table [Tên bảng]
     * @param  [array] $where [mảng điều kiện]
     * @return [array]        [mảng dữ liệu trả về nếu có hoặc một mảng rỗng]
     */
    public function get_rows(){
        self::build_sql();
        $result = $this->conn->query($this->sql);
        $data = array();
        // Kiểm tra dữ liệu trả về có hoặc không
        if ($result !== false && $result->num_rows > 0 ) {
            // Gán dữ liệu vào mảng trả về
            while($row = $result->fetch_assoc()) {
                $data[] = $row;
            }
        }
        return $data;
    }



    /**
     * [count_all Đếm số lượng bản ghi theo điều kiện]
     * @param  [string] $table [Tên bảng]
     * @param  [array] $where [mảng điều kiện]
     * @return [array]        [sô lượng bản ghi đếm đc]
     */
    public function count_all($where){
        $_sql = "SELECT COUNT(*) as total_res FROM ". $this->table;
        $_sql .= self::build_where($where);
        $result = $this->conn->query($_sql);
        if ($result !== false && $result->num_rows > 0)
            return (int) $result->fetch_assoc()['total_res'];
        else 
            return 0;
    }

    protected function build_where($_where){
        if (!is_array($_where) && $_where == 1) return "";
        if (!is_array($_where) && $_where != 1) return " WHERE ".$_where;
        $condition = " WHERE ";
        $i = 0;
        // Xử lý mảng điều kiện
        foreach ($_where as $key => $value) {
            if ($key == 1) {
                return '';
            }
            $value = $this->conn->real_escape_string ($value);
            $value = $this->conn->real_escape_string ($value);
            // Gán điều kiện
            if (strpos('<', $key) || strpos('>', $key)) {
                if (is_int($value)) {
                    $condition .= $key . " " . $value;
                } else {
                    $condition .= $key . " '" . $value . "' ";
                }
            } elseif (strpos(strtolower($key), 'or_like') != false) {
                $condition .= explode('or_like', $key)[0] . " LIKE '%" . $value . "%' ";
            } elseif (strpos(strtolower($key), 'like') != false) {
                $condition .= explode('like', $key)[0] . " LIKE '%" . $value . "%' ";
            } else {
                $condition .= $key . "='" . $value . "' ";
            }


            // Nếu mảng chỉ có một phần tử thì không thêm dấu ','
            // EX: "SELECT * FROM 'table' WHERE ID=1;"
            if (count($_where) < 2) {
                break;
            }
            // Thêm liên từ
            elseif (++$i !== count($_where)) {
                $condition .= strpos(strtolower($key), 'or_like') ? 'OR ' : "AND ";
            }
        }

        return $condition;
    }
    /**
     * [update Cập nhật một bản ghi]
     * @param  [string] $table [tên bảng]
     * @param  [array] $data  [thông tin cập nhật]
     * @param  [array] $where [Điều kiện]
     * @return [boolean]        [trạng thái cập nhật(true/false)]
     */
    public function update($data, $where){

        $sql = "UPDATE `".$this->table."` SET ";
        $i = 0;
        // Set thông tin cập nhật
        foreach ($data as $key => $value) {
            $sql .= "`" . $key . "`='" . $value ."' ";
            if (count($data) < 2) {
                break;
            }
            elseif (++$i !== count($data)) {
                $sql .= ", ";
            }
            
        }

        $sql .= self::build_where($where);
        return $this->conn->query($sql);

    }


    /**
     * [delete Xóa một bản ghi]
     * @param  [string] $table [tên bảng]
     * @param  [array] $where Mảng các điều kiện ràng buộc
     * @return [boolean]        [trạng thái thực thi lệnh(true/false)]
     */
    public function delete($where){

        $sql = "DELETE FROM ". $this->table . " ";
        $sql .= self::build_where($where);
        return $this->conn->query($sql);
    }


    public function __destruct(){
        $this->conn->close();
    }
}

 ?>