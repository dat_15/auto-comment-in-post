<?php
/**
* Author : Nguyen Tien Dat.
*/
abstract class Controller
{
    private $data;
    protected $name;
    protected $userInfo;

    public function __construct()
    {
        $this->name = strtolower(get_class($this));
        self::check_auth();
    }

    protected function __get($varName)
    {
        if (!array_key_exists($varName, $this->data)) {
            //this attribute is not defined!
            throw new Exception();
        } else {
            return $this->data[$varName];
        }
    }

    /**
     * load model và tự tạo biến mới.
     *
     * @param [string] $model [tên model]
     *
     * @return [object] [null hoặc model]
     */
    protected function load_model($name = '')
    {
        if ($name == '') {
            $name = $this->name;
        }
        if (file_exists('models/'.strtolower($name).'.php')) {
            require 'models/'.strtolower($name).'.php';
            $cls1 = $name;

            return new $cls1();
        } else {
            echo 'Model '.$name.' does not existed';

            return;
        }
    }

    /**
     * Load view trên controller, biến sử dụng bên dạng $ + index của mảng
     * Ex: Controller: $data['title'] = "Page title";
     *     View: echo $title.
     *
     * @param [string] $view  [tên view]
     * @param array    $param [mảng chứa các biến truyền vào view]
     *
     * @return [null] [nếu không tìm thấy file sẽ trả về null]
     */
    protected function load_view($view, $param = array())
    {
        if ($module == '') {
            $module = $this->name;
        }
        if (file_exists('views/'.$view.'.php')) {
            if (!is_array($param)) {
                $param = is_object($param) ? get_object_vars($param) : array();
            }
            extract($param);
            // include file
            include_once 'views/'.$view.'.php';
        } else {
            echo 'View '.$view.' does not existed';

            return;
        }
    }

    protected function randomToken($length)
    {
        $characters = 'abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; ++$i) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }

    protected function check_auth()
    {
        $isLoggedIn = Session::get_session('isLoggedIn');
        $uid = Session::get_session('uid');
        $email = Session::get_session('email');
        $uid = (int) de_id($uid);
        if ($isLoggedIn !== true) {
            return false;
        } elseif (empty($uid) || empty($email)) {
            return false;
        } else {
            $table = USER_TABLE;
            $db = new DB($table);
            $checkUser = $db->where([
              'email' => $email,
              'uid' => $uid,
            ])->get_row($table);
            if (!count($checkUser)) {
                return false;
            }
            $this->userInfo = $checkUser;
        }
        return true;
    }

    protected function check_valid_email($email)
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    protected function check_valid_phone_number($phonenumber)
    {
        return preg_match('/^0(1\d{9}|9\d{8})$/', $phonenumber);
    }

    protected function check_valid_password($password)
    {
        return preg_match('/^(?=.*[A-Z])(?=.*[!@#_])(?=.*[0-9])(?=.*[a-z]).{8,30}$/', $password);
    }

    protected function check_valid_username($username)
    {
        return preg_match('/^[a-zA-Z0-9_]{8,30}$/', $username);
    }

    protected function check_valid_fullname($fullname)
    {
        return preg_match('/^[ÀÁẠẢÃÂẦẤẬẨẪĂẮẲẶẴẰÈÉẸẺẼÊỀẾỆỂỄÌÍỊỈĨÒÓỌỎÕÔỒỐỘỔỖƠỜỚỢỞỠÙÚỤỦŨƯỪỨỰỬỮỲÝỴỶỸĐàáạảãâầấậẩẫăằắặẳẵèéẹẻẽêềếệểễìíịỉĩòóọỏõôồốộổỗơờớợởỡùúụủũưừứựửữỳýỵỷỹđa-zA-Z ]{8,30}$/', $fullname);
    }

    protected function check_valid_fb_id($fb_id)
    {
        return preg_match('/^[0-9_]{16,33}$/', $fb_id);
    }

    protected function check_valid_sirname($sirname)
    {
        return preg_match('/^[ÀÁẠẢÃÂẦẤẬẨẪĂẮẲẶẴẰÈÉẸẺẼÊỀẾỆỂỄÌÍỊỈĨÒÓỌỎÕÔỒỐỘỔỖƠỜỚỢỞỠÙÚỤỦŨƯỪỨỰỬỮỲÝỴỶỸĐàáạảãâầấậẩẫăằắặẳẵèéẹẻẽêềếệểễìíịỉĩòóọỏõôồốộổỗơờớợởỡùúụủũưừứựửữỳýỵỷỹđa-zA-Z ]{2,25}$/', $sirname);
    }

    protected function get_client_ip() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
           $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }
}
