<?php


/**
 * Biến chung.
 */

// Đường dẫn
// define('base_url', 'https://tudongbinhluan.com/');
define('base_url', 'https://localhost/dengo/');
define('ADMIN_PAGE', base_url.'manage/main');
define('USER_LOGIN', base_url.'login');
define('FORGOT_PASSWORD', base_url.'forgot-password');
define('USER_REGISTER', base_url.'register');
define('LOGOUT', base_url.'logout');

// Web config
define('HOME_CONTROLLER', 'home');
define('PER_PAGE', 10);
define('ENCODE_1', 985200459);
define('ENCODE_2', 7713);
define('NAME_PATTERN', '[ÀÁẠẢÃÂẦẤẬẨẪĂẮẲẶẴẰÈÉẸẺẼÊỀẾỆỂỄÌÍỊỈĨÒÓỌỎÕÔỒỐỘỔỖƠỜỚỢỞỠÙÚỤỦŨƯỪỨỰỬỮỲÝỴỶỸĐàáạảãâầấậẩẫăằắặẳẵèéẹẻẽêềếệểễìíịỉĩòóọỏõôồốộổỗơờớợởỡùúụủũưừứựửữỳýỵỷỹđa-zA-Z ]{2,25}');
define('PASSWORD_PATTERN', '(?=.*[A-Z])(?=.*[!@#_])(?=.*[0-9])(?=.*[a-z]).{8,30}');
define('USERNAME_PATTERN', '[a-zA-Z0-9_]{8,30}');

/*
 * Bảng trong DB
 */
define('USER_TABLE', '_ac_users');
define('FB_ACCOUNT_TABLE', '_ac_facebook_account');
define('FP_ACCOUNT_TABLE', '_ac_fanpage_account');
define('FP_POST_TABLE', '_ac_facebook_post');
define('APP_LOG_TABLE', '_ac_app_log');
