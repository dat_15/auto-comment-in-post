<?php


/*
 * en - de : code id
 */
function en_id($id){
    $id = intval($id) * ENCODE_2; //  15000
    $id = dechex($id + ENCODE_1);
    $id = str_replace(1, 'J', $id);
    $id = str_replace(6, 'V', $id);
    $id = str_replace(8, 'S', $id);
    $id = str_replace(9, 'R', $id);
    $id = str_replace(7, 'T', $id);

    return strtoupper($id);
}
function de_id($id){
    $id = str_replace('J', 1, $id);
    $id = str_replace('V', 6, $id);
    $id = str_replace('S', 8, $id);
    $id = str_replace('R', 9, $id);
    $id = str_replace('T', 7, $id);
    $id = (hexdec($id) - ENCODE_1) / ENCODE_2;

    return $id;
}

function input($k = '*', $type = 'GP', $default = null){
    $type = strtoupper($type);
    switch ($type) {
        case 'G': $var = $_GET; break;
        case 'P': $var = &$_POST; break;
        case 'C': $var = &$_COOKIE; break;
        default:
            if (isset($_GET[$k])) {
                $var = &$_GET;
            } else {
                $var = &$_POST;
            }
            break;
    }

    $val = isset($var[$k]) ? $var[$k] : $default;
    if ($k === '*') {
        $val = &$_POST;
    }
    if ($key === '*' && $type == 'G') {
        $val = &$_GET;
    }
    if (!is_array($val)) {
        $val = cleanXssAndSqlInjection(trim($val, " \n"));
    } else {
        foreach ($val as &$value) {
            if (!is_array($value)) {
                $value = cleanXssAndSqlInjection(trim($value, " \n"));
            }
        }
    }

    return $val;
}

function cleanXssAndSqlInjection($val){
    $searchXss = array(
        '@SLEEP@si',
        '@sleep@si',

        '@</script[^>]*?>@si',
        '@<script[^>]*?>@si',
        '@<script[^>]*?>.*?</script>@si',   // Strip out javascript
        '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
        '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments
        , '@alert@isU', '@prompt@isU', '@window.location@isU', '@FSCommand@isU', '@onAbort@isU', '@onActivate@isU', '@onAfterPrint@isU', '@onAfterUpdate@isU', '@onBeforeActivate@isU',
        '@onBeforeCopy@isU', '@onBeforeCut@isU', '@onBeforeDeactivate@isU', '@onBeforeEditFocus@isU', '@onBeforePaste@isU',
        '@onBeforePrint@isU', '@onBeforeUnload@isU', '@onBeforeUpdate@isU', '@onBegin@isU', '@onBlur@isU', '@onBounce@isU',
        '@onCellChange@isU', '@onChange@isU', '@onClick@isU', '@onContextMenu@isU', '@onControlSelect@isU', '@onCopy@isU',
        '@onCut@isU', '@onDataAvailable@isU', '@onDataSetChanged@isU', '@onDataSetComplete@isU', '@onDblClick@isU', '@onDeactivate@isU',
        '@onDrag@isU', '@onDragEnd@isU', '@onDragLeave@isU', '@onDragEnter@isU', '@onDragOver@isU', '@onDragDrop@isU', '@onDragStart@isU',
        '@onDrop@isU', '@onEnd@isU', '@onError@isU', '@onErrorUpdate@isU', '@onFilterChange@isU', '@onFinish@isU', '@onFocus@isU', '@onFocusIn@isU',
        '@onFocusOut@isU', '@onHashChange@isU', '@onHelp@isU', '@onInput@isU', '@onKeyDown@isU', '@onKeyPress@isU', '@onKeyUp@isU', '@onLayoutComplete@isU',
        '@onLoad@isU', '@onLoseCapture@isU', '@onMediaComplete@isU', '@onMediaError@isU', '@onMessage@isU', '@onMouseDown@isU', '@onMouseEnter@isU',
        '@onMouseLeave@isU', '@onMouseMove@isU', '@onMouseOut@isU', '@onMouseOver@isU', '@onMouseUp@isU', '@onMouseWheel@isU', '@onMove@isU',
        '@onMoveEnd@isU', '@onMoveStart@isU', '@onOutOfSync@isU', '@onPaste@isU', '@onPause@isU', '@onPopState@isU',
        '@onProgress@isU', '@onPropertyChange@isU', '@onReadyStateChange@isU', '@onRedo@isU', '@onRepeat@isU', '@onReset@isU', '@onResize@isU', '@onResizeEnd@isU',
        '@onResizeStart@isU', '@onResume@isU', '@onReverse@isU', '@onRowsEnter@isU', '@onRowExit@isU', '@onRowDeletet@isU', '@onRowInserted@isU', '@onScroll@isU',
        '@onSeek@isU', '@onSelect@isU', '@onSelectionChange@isU', '@onSelectStart@isU', '@onStart@isU', '@onStorage@isU', '@onSyncRestored@isU', '@onSubmit@isU',
        '@onTimeErrort@isU', '@onTrackChange@isU', '@onUndo@isU', '@onUnload@isU', '@onURLFlip@isU', '@seekSegmentTime@isU', '@iframe@isU',
    );
    $val = preg_replace($searchXss, '', $val);

    $searchSqlKey = array(
        '@database@isU',
        /*'@ OR@isU',*/
        /*'@OR @isU',*/
        '@BENCHMARK@isU',
        '@DROP@isU',
        '@DROP/*@isU',
        '@/\*@isU',
        '@\*/@isU',
        '@CONCAT@isU',
        '@UNION @isU',
        '@LOAD_FILE@isU',
        '@CREATE @isU',
        '@GRANT@isU',
        '@PRIVILEGES@isU',
        '@information_schema@isU',
        '@datadir@isU',
        '@CHR @isU',
        '@WAITFOR@isU',
        '@OUTFILE@isU',
        /*'@0x@isU',*/
        '@"HEX@isU',
        '@ASCII@isU',
        '@SUBSTRING@isU',
    );

    return preg_replace($searchSqlKey, '', $val);
}

function send_json($data, $no_die = false){
    header('Content-Type: application/json;charset=utf-8');
    $json = json_encode($data, JSON_PRETTY_PRINT);
    if ($json === false) {
        $json = json_encode(convert_from_latin1_to_utf8_recursively($data));
        if ($json === false) {
            $json = '{"jsonError": "unknown"}';
        }
        http_response_code(500);
    }
    echo $json;
    if ($no_die === false) {
        die();
    }
}

function convert_from_latin1_to_utf8_recursively($dat){
    if (is_string($dat)) {
        return utf8_encode($dat);
    } elseif (is_array($dat)) {
        $ret = [];
        foreach ($dat as $i => $d) {
            $ret[$i] = convert_from_latin1_to_utf8_recursively($d);
        }

        return $ret;
    } elseif (is_object($dat)) {
        foreach ($dat as $i => $d) {
            $dat->$i = convert_from_latin1_to_utf8_recursively($d);
        }

        return $dat;
    } else {
        return $dat;
    }
}

// Chuyển đổi chuỗi tiếng Việt có dấu sang không dấu
function vn_transfer_string($text){
    $text = str_replace('/', '-', $text);
    $text = str_replace('"', '', $text);
    $text = strtolower($text);
    $utf8 = array(
        '/[áàâãăạảắẳẵằặấầẩẫậ]/u' => 'a',
        '/[íìịĩỉ]/u' => 'i',
        '/[éèêẹẽếềễệẻể]/u' => 'e',
        '/[óòôõọỏơờởớợỡồổốộ]/u' => 'o',
        '/[úùũụủưứừửữự]/u' => 'u',
        '/[ýỹỷỳỵ]/u' => 'y',
        '/[đ]/u' => 'd',
        '/–/' => '-', // UTF-8 hyphen to "normal" hyphen
        '/[’‘‹›‚]/u' => '', // Literally a single quote
        '/[“”«»„]/u' => '', // Double quote
    );
    if ($remove_space) {
        $utf8['/ /'] = '-';
    }

    return preg_replace(array_keys($utf8), array_values($utf8), $text);
}

function get_assets_file($file){
    return base_url.'assets/'.$file.'?v='.time();
}

function CURL_REQUEST($url, $method = 'GET', $data = array(), $cookie = false, $timeout = 60){
    try {
        $ch = curl_init();
        $time_start = microtime(true);
        if (isset($cookie) && $cookie != '') {
            curl_setopt($ch, CURLOPT_COOKIE, $cookie);
        }
        //var_dump($cookie);
        if ($method == 'POST') {
            curl_setopt($ch, CURLOPT_POST, 1);
            if (is_array($data) && sizeof($data) == 0) {
                $data['params_default_324324'] = 1;
            }
        }

        if ($method == 'DELETE') {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
        }

        if (sizeof($data) > 0) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        }

        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $domain = base_url;
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Origin: '.$domain, 'Referer: '.$domain));
        // curl_setopt($ch, CURLOPT_USERAGENT, $headerinfo['User-Agent']);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        $rs = curl_exec($ch);
        curl_close($ch);

        return $rs;
    } catch (Exception $e) {
        return $e->getMessage();
    }
}
