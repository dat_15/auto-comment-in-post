<?php
class fb_post extends Model {
    function __construct(){
        parent::__construct(FP_POST_TABLE);
    }

    public function get_all_availables($uid){
    	return $this->db->query("SELECT post.* FROM " . FP_POST_TABLE . " post join " . FP_ACCOUNT_TABLE . " fb ON post.parent_id=fb.fanpage_id WHERE fb.uid=$uid AND fb.is_subscribed=1 ORDER BY post.parent_id");
    }

    public function get_post_with_fanpage($post_id){
    	return $this->db->query("SELECT post.comment_reply,post.message_reply,fb.page_token,fb.fanpage_id,fb.uid FROM " . FP_POST_TABLE . " post join " . FP_ACCOUNT_TABLE . " fb ON post.parent_id=fb.fanpage_id WHERE post.post_id='$post_id' AND post.is_running=1 AND fb.is_subscribed=1 ORDER BY post.parent_id");
    }
}
?>