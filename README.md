#Naming convension
 - Toàn bộ tên file trong controller đều phải viết thường, ngăn cách bởi dấu '_' (dash)
 - Tên class viết thường và cũng cách nhau bở các gạch dưới
 - Tên model cũng như class controller
 - Với các template của views thì có thể đặt trong các thư mục riêng để dễ quản lý.

#Controller
 - Mặc định phải có hàm __construct
 - Load model bằng hàm load_model của instance. VD: $this->products = $this->load_model('products');
 - Load view và truyền biến đến view thông qua hàm load_view của instance.
 	VD: $this->load_view('common/header', ['title' => 'Kangaroo - MÁy lọc nước hàng đầu Việt Nam']);
	Hàm này sẽ tìm đến thư mục common trong thư mục views và load file header.php và truyền biến title vào file header này.
	Trong file header có thể in biến này bằng thẻ nhúng của PHP: <?php echo $title; ?>

#Model
 - Tôi có viết một class Model để các model con extend lại. Trong mother class này đã chứa các truy vấn cơ bản để giúp mình thêm, sửa, xóa, tìm kiếm, phân trang với từng model.
 - Mỗi model là một bảng trong DB. Khởi tạo cũng như model của products 
 
#Views
 - Từng view sẽ load css và js riêng của nó. Load file bằng hàm get_assets_file(url), version sẽ đc render tự động
  	VD: <link rel="stylesheet" href="<?php echo get_assets_file('assets/css/header.css');?>">