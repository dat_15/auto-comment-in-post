<?php

class login extends Controller
{
    private $session;
    private $user;

    public function __construct()
    {
        parent::__construct(__CLASS__);
        $this->session = new Session();
        $this->user = $this->load_model('user');
        if (count($this->userInfo)) {
            header('Location: '.$this->session->get_session('goToUrl'));
            die;
        }
    }

    public function index()
    {      
        $token = md5($this->randomToken(10).time());
        $this->session->set_session('login-token', $token);
        $this->load_view('common/header', [
            'title' => 'Đăng nhập',
            'page' => 'login'
        ]);
        $this->load_view('common/login', [
            'token' => $token,
            'msg' => $this->session->get_flash_session('flash-message')
        ]);
        $this->load_view('common/footer',['not_get_js' => true]);
    }
}
