<?php
/**
* index controller.
*/
class register extends Controller
{
    private $session;
    private $user;
    private $callbackUrl;

    public function __construct()
    {
        parent::__construct(__CLASS__);
        $this->session = new Session();
        if (count($this->userInfo)) {
            header('Location: '.$this->session->get_session('goToUrl'));
            die();
        }
        $this->session->set_session('register-token', md5($this->randomToken(10).time()));
    }

    public function index()
    {
        $this->load_view('common/header', [
            'title' => 'Đăng ký tài khoản mới',
            'page' => 'login'
        ]);
        $this->load_view('common/register', ['token' => $this->session->get_session('register-token')]);
        $this->load_view('common/footer', ['not_get_js' => true]);
    }
}
