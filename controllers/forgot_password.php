<?php

class forgot_password extends Controller
{
    private $session;
    private $user;

    public function __construct()
    {
        parent::__construct(__CLASS__);
        $this->session = new Session();
        $this->user = $this->load_model('user');
        if (count($this->userInfo)) {
            header('Location: '.$this->session->get_session('goToUrl'));
            die;
        }
    }

    public function index()
    {      
        $token = md5($this->randomToken(10).time());
        $this->session->set_session('forgot-password-token', $token);
        $this->load_view('common/header', [
            'title' => 'Quên mật khẩu',
            'page' => 'login'
        ]);
        $this->load_view('common/forgot-pass', [
            'token' => $token,
            'msg' => 'Vui lòng điền email đã đăng ký tài khoản.<br>Chúng tôi sẽ gửi đường dẫn tạo mật khẩu mới đến email này.'
        ]);
        $this->load_view('common/footer',['not_get_js' => true]);
    }
}
