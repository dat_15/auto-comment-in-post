<?php

class dieu_khoan_su_dung extends Controller
{
    private $model;

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->load_view('common/header', [
            'title' => 'Điều khoản sử dụng',
            'page' => 'dieu-khoan-su-dung'
        ]);
        $this->load_view('common/dieu-khoan-su-dung');
        $this->load_view('common/footer');
    }
}
