<?php

class content extends Controller
{
    private $session;
    private $user;

    public function __construct()
    {
        parent::__construct(__CLASS__);
        $this->session = new Session();
        $this->user = $this->load_model('user');
        if (!count($this->userInfo)) {
            header('Location: '.USER_LOGIN);
            die;
        }
    }

    public function index(){
        $token = md5($this->randomToken(10).time());
        $this->session->set_session('content-token', $token);
        $this->load_view('admin/header', [
            'title' => 'Thiết lập nội dung trả lời', 
            'user' => $this->userInfo
        ]);
        $this->load_view('admin/set-content', [
            'token' => $token,
            'msg' => $this->session->get_flash_session('flash-message')
        ]);
        $this->load_view('admin/footer');
    }
}
