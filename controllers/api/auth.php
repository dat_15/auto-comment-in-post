<?php

class auth extends Controller
{
    private $session;
    
    public function __construct()
    {
        parent::__construct();
        $this->session = new Session();
        $this->user = $this->load_model('user');
    }

    public function check_username_exist(){
        $token = input('token', 'p', '');
        $username = input('username', 'p', '');
        if(empty($token)){
            send_json([
                'status' => 0,
                'msg' => 'Thiếu token'
            ]);
        }
        if ($token !== $this->session->get_session('register-token')) {
            send_json([
                'status' => 0,
                'msg' => 'token không hợp lệ'
            ]);
        }
        if(empty($username)){
            send_json([
                'status' => 0,
                'msg' => 'Thiếu tên đăng nhập'
            ]);
        }
        if(!$this->check_valid_username($username)){
            send_json([
                'status' => 0,
                'msg' => 'username không hợp lệ'
            ]);
        }
        send_json([
            'status' => 1,
            'is_exist' => count($this->user->get_row(['username' => $username]))
        ]);
        die;
    }

    public function check_email_exist(){
        $token = input('token', 'p', '');
        $email = input('email', 'p', '');
        if(empty($token)){
            send_json([
                'status' => 0,
                'msg' => 'Thiếu token'
            ]);
        }
        if ($token !== $this->session->get_session('register-token')) {
            send_json([
                'status' => 0,
                'msg' => 'token không hợp lệ'
            ]);
        }
        if(empty($email)){
            send_json([
                'status' => 0,
                'msg' => 'Thiếu email'
            ]);
        }
        if(!$this->check_valid_email($email)){
            send_json([
                'status' => 0,
                'msg' => 'email không hợp lệ'
            ]);
        }
        $email = trim($email);
        send_json([
            'status' => 1,
            'is_exist' => count($this->user->get_row(['email' => $email]))
        ]);
    }

    public function do_registration(){
        $token = input('token', 'p', '');
        $first_name = input('first_name', 'p', '');
        $last_name = input('last_name', 'p', '');
        $username = input('username', 'p', '');
        $email = input('email', 'p', '');
        $password = input('password', 'p', '');
        $re_password = input('re_password', 'p', '');
        if(empty($token)){
            send_json([
                'status' => 0,
                'msg' => 'Thiếu token'
            ]);
        }
        if ($token !== $this->session->get_session('register-token')) {
            send_json([
                'status' => 0,
                'msg' => 'Token không khớp'
            ]);
        }
        if(empty($email)){
            send_json([
                'status' => 0,
                'msg' => 'Thiếu email'
            ]);
        }
        if(!$this->check_valid_email($email)){
            send_json([
                'status' => 0,
                'msg' => 'Email không hợp lệ'
            ]);
        }
        if(empty($first_name)){
            send_json([
                'status' => 0,
                'msg' => 'Thiếu Họ'
            ]);
        }
        if(!$this->check_valid_sirname($first_name)){
            send_json([
                'status' => 0,
                'msg' => 'Họ không hợp lệ'
            ]);
        }
        if(empty($last_name)){
            send_json([
                'status' => 0,
                'msg' => 'Thiếu Tên'
            ]);
        }
        if(!$this->check_valid_sirname($last_name)){
            send_json([
                'status' => 0,
                'msg' => 'Tên không hợp lệ'
            ]);
        }
        if(empty($username)){
            send_json([
                'status' => 0,
                'msg' => 'Thiếu tên đăng nhập'
            ]);
        }
        if(!$this->check_valid_username($username)){
            send_json([
                'status' => 0,
                'msg' => 'Tên đăng nhập không hợp lệ'
            ]);
        }
        if(empty($password)){
            send_json([
                'status' => 0,
                'msg' => 'Thiếu mật khẩu'
            ]);
        }
        if(!$this->check_valid_password($password)){
            send_json([
                'status' => 0,
                'msg' => 'Mật khẩu không hợp lệ',
            ]);
        }
        if($password !== $re_password){
            send_json([
                'status' => 0,
                'msg' => 'Mật khẩu nhập lại không khớp'
            ]);
        }
        $uid = $this->user->add([
            'username' => $username,
            'first_name' => $first_name,
            'last_name' => $last_name,
            'email' => $email,
            'password' => password_hash($password, PASSWORD_BCRYPT, ['cost' => 12])
        ]);
        if($uid) $this->session->set_flash_message('flash-message','Tạo tài khoản thành công.<br> Vui lòng đăng nhập');
        send_json([
            'status' => $uid ? 1 : 0,
            'msg' => $uid ? 'Tạo tài khoản thành công.<br>Chúng tôi sẽ chuyển hướng bạn trong dây lát' : 'Tạo tài khoản thất bại',
            'callback_url' => USER_LOGIN
        ]);
    }

    public function do_login(){
        $token = input('token', 'p', '');
        $username = input('username', 'p', '');
        $password = input('password', 'p', '');
        if(empty($token)){
            send_json([
                'status' => 0,
                'msg' => 'Thiếu token'
            ]);
        }
        if ($token !== $this->session->get_session('login-token')) {
            send_json([
                'status' => 0,
                'msg' => 'Token không khớp'
            ]);
        }
        
        if(empty($username)){
            send_json([
                'status' => 0,
                'msg' => 'Thiếu tên đăng nhập'
            ]);
        }
        if(!$this->check_valid_username($username)){
            send_json([
                'status' => 0,
                'msg' => 'Tên đăng nhập không hợp lệ'
            ]);
        }
        if(empty($password)){
            send_json([
                'status' => 0,
                'msg' => 'Thiếu mật khẩu'
            ]);
        }
        if(!$this->check_valid_password($password)){
            send_json([
                'status' => 0,
                'msg' => 'Mật khẩu không hợp lệ',
            ]);
        }
        $user = $this->user->get_row(['username' => $username]);
        if(!empty($user) && count($user)){
            if (password_verify($password, $user['password'])){
                $this->session->set_session('isLoggedIn', true);
                $this->session->set_session('uid', en_id($user['uid']) );
                $this->session->set_session('email', $user['email']);
                $this->session->set_flash_message('flash-message','Đăng nhập thành công.');
                $callback_url = $this->session->get_session('goToUrl');
                if(empty($callback_url)) $callback_url = ADMIN_PAGE;
                send_json([
                    'status'        => 1,
                    'msg'           => 'Đăng nhập thành công<br>Chúng tôi sẽ chuyển hướng bạn trong giây lát',
                    'callback_url'  => $callback_url
                ]);
            }else{
                send_json([
                    'status' => 0,
                    'msg'    => 'Tài khoản không đúng.<br> Vui lòng xem lại'
                ]);
            }
        }        
        send_json([
            'status' => 0,
            'msg'    => 'Tài khoản không tồn tại <br> Vui lòng xem lại'
        ]);
    }
}
