<?php

class facebook extends Controller
{
    private $v;
    private $session;
    private $user;
    private $fb_account;
    private $fb_post;
    private $app_log;
    private $app_id;
    private $app_secret;
    public function __construct()
    {
        parent::__construct(__CLASS__);
        $this->session = new Session();
        $this->user = $this->load_model('user');
        $this->fb_account = $this->load_model('fb_account');
        $this->fanpage_account = $this->load_model('fanpage_account');
        $this->fb_post = $this->load_model('fb_post');
        $this->app_log = $this->load_model('app_log');
        $this->v = 'v3.0';
        $this->app_id = '196731451008310';
        $this->app_secret = 'b81666ff1d7a56d76dcd55b73985f6d6';
    }

    /**
     * Lấy danh sách fanpage của tài khoản đã kết nối
     */
    public function get_fb_page($token){
        if (!count($this->userInfo)) {
            send_json([
                'status' => 0,
                'msg' => 'Bạn chưa đăng nhập'
            ]);
        }
        if(empty($token)){
            send_json([
                'status' => 0,
                'msg' => 'Thiếu token'
            ]);
        }
        if ($token !== $this->session->get_session('main-token')) {
            send_json([
                'status' => 0,
                'msg' => 'Token không hợp lệ'
            ]);
        }
        $account_info = $this->fb_account->get_row(['uid' => $this->userInfo['uid']]);
        if(empty($account_info)){
            send_json([
                'status' => 0,
                'msg' => 'Tài khoản chưa được kết nối'
            ]);
        }else{
            $pages = $this->fanpage_account->filter(['uid' => $this->userInfo['uid']]);
            if( (int) $page['total'] > 0){
                foreach ($pages['data'] as &$p) {
                    $p = [
                        'category' => $p['category'],
                        'fanpage_id' => $p['fanpage_id'],
                        'fanpage_name' => $p['fanpage_name'],
                        'is_subscribed' => $p['is_subscribed']
                    ];
                }
                send_json([
                    'status' => 1,
                    'msg' => 'success',
                    'data' => $pages
                ]);
            }else{
                $url = 'https://graph.facebook.com/'.$this->v.'/'.$account_info['facebook_id'].'?fields=accounts&access_token='.$account_info['user_token'] . '&limit=50';
                $res = json_decode(CURL_REQUEST($url, 'GET'));
                if( isset($res->accounts->data) && is_array($res->accounts->data) ){
                    foreach ($res->accounts->data as $p) {
                        $this->fanpage_account->add([
                            'uid' => $this->userInfo['uid'],
                            'fanpage_id' => $p->id,
                            'fanpage_name' => $p->name,
                            'page_token' => $p->access_token,
                            'category' => $p->category,
                            'perms' => implode(',', $p->perms)
                        ]);
                    }
                }
                $pages = $this->fanpage_account->filter(['uid' => $this->userInfo['uid']]);
                foreach ($pages['data'] as &$p) {
                    $p = [
                        'category' => $p['category'],
                        'fanpage_id' => $p['fanpage_id'],
                        'fanpage_name' => $p['fanpage_name'],
                        'is_subscribed' => $p['is_subscribed']
                    ];
                }
                send_json([
                    'status' => 1,
                    'msg' => 'success',
                    'data' => $pages
                ]);
                die();
            }
        }
    }

    /**
     * Đăng ký fanpage nhận webhook từ facebook
     * sử dụng graph API
     */
    public function subscribe_app($page_id, $token){
        if (!count($this->userInfo)) {
            send_json([
                'status' => 0,
                'msg' => 'Bạn chưa đăng nhập'
            ]);
        }
        if(empty($token)){
            send_json([
                'status' => 0,
                'msg' => 'Thiếu token'
            ]);
        }
        if ($token !== $this->session->get_session('main-token')) {
            send_json([
                'status' => 0,
                'msg' => 'Token không hợp lệ'
            ]);
        }
        if(empty($page_id)){
            send_json([
                'status' => 0,
                'msg' => 'Thiếu fanpage id'
            ]);
        }
        if(!$this->check_valid_fb_id($page_id)){
            send_json([
                'status' => 0,
                'msg' => 'Fanpage id không hợp lệ'
            ]);
        }
        $account_info = $this->fb_account->get_row(['uid' => $this->userInfo['uid']]);
        if(empty($account_info)){
            send_json([
                'status' => 0,
                'msg' => 'Tài khoản chưa được kết nối'
            ]);
        }
        $page_info = $this->fanpage_account->get_row(['uid' => $this->userInfo['uid'],'fanpage_id' => $page_id]);
        if(empty($page_info) || !count($page_info)){
            send_json([
                'status' => 0,
                'msg' => 'Fanpage không tồn tại'
            ]);
        }
        if( (int) $page_info['is_subscribed'] == 1){
            send_json([
                'status' => 0,
                'msg' => 'Fanpage đã được đăng ký'
            ]);
        }
        $url = 'https://graph.facebook.com/'.$this->v.'/'.$page_id.'/subscribed_apps?access_token='.$page_info['page_token'];
        $data = ['access_token' => $page_info['page_token']];
        $res = CURL_REQUEST($url, 'POST', $data);
        if(strpos($res, 'true')){
            $this->fanpage_account->update([
                'is_subscribed' => 1
            ], [
                'fanpage_id' => $page_id,
                'page_token' => $page_info['page_token']
            ]);
        }
        echo $res;
        die;
    }

    /**
     * Hủy đăng ký cho fanpage
     * sử dụng graph API
     */
    public function unsubscribe_app($page_id, $token){
        if (!count($this->userInfo)) {
            send_json([
                'status' => 0,
                'msg' => 'Bạn chưa đăng nhập'
            ]);
        }
        if(empty($token)){
            send_json([
                'status' => 0,
                'msg' => 'Thiếu token'
            ]);
        }
        if ($token !== $this->session->get_session('main-token')) {
            send_json([
                'status' => 0,
                'msg' => 'Token không hợp lệ'
            ]);
        }
        if(empty($page_id)){
            send_json([
                'status' => 0,
                'msg' => 'Thiếu fanpage id'
            ]);
        }
        if(!$this->check_valid_fb_id($page_id)){
            send_json([
                'status' => 0,
                'msg' => 'Fanpage id không hợp lệ'
            ]);
        }
        $account_info = $this->fb_account->get_row(['uid' => $this->userInfo['uid']]);
        if(empty($account_info)){
            send_json([
                'status' => 0,
                'msg' => 'Tài khoản chưa được kết nối'
            ]);
        }
        $page_info = $this->fanpage_account->get_row(['uid' => $this->userInfo['uid'],'fanpage_id' => $page_id]);
        if(empty($page_info) || !count($page_info)){
            send_json([
                'status' => 0,
                'msg' => 'Fanpage không tồn tại'
            ]);
        }
        if( (int) $page_info['is_subscribed'] == 0){
            send_json([
                'status' => 0,
                'msg' => 'Fanpage chưa được đăng ký'
            ]);
        }
        $url = 'https://graph.facebook.com/'.$this->v.'/'.$page_id.'/subscribed_apps?access_token='.$page_info['page_token'];
        $data = ['access_token' => $page_info['page_token']];
        $res = CURL_REQUEST($url, 'DELETE', $data);
        if(strpos($res, 'true')){
            $this->fanpage_account->update([
                'is_subscribed' => 0
            ], [
                'fanpage_id' => $page_id,
                'page_token' => $page_info['page_token']
            ]);
        }
        echo $res;
        die;
    }

    /**
     * Lấy token dài hạn từ token ngắn hạn
     * sử dụng graph API
     */
    private function get_long_lived_token($token){
        $url = "https://graph.facebook.com/".$this->v."/oauth/access_token?grant_type=fb_exchange_token&client_id=".$this->app_id."&client_secret=".$this->app_secret."&fb_exchange_token=$token";
        return CURL_REQUEST($url, 'GET');
    }

    /**
     * Nhận thông báo từ facebook
     */
    public function webhook_fb(){
        if (isset($_GET['hub_mode']) && isset($_GET['hub_challenge']) && isset($_GET['hub_verify_token'])) {
            if ($_GET['hub_verify_token'] === 'auto_comment_reply_token') {
                echo $_GET['hub_challenge'];
            }
        } else {
            $feedData = file_get_contents('php://input');
            $data = json_decode($feedData);
            if (empty($feedData)) {
                $log_data = [
                    'uid' => -1,
                    'post_id' => -1,
                    'page_id' => -1,
                    'comment_data' => 'Kiểm tra IP: ' . $this->get_client_ip(), 
                    'comment_reply' => 'Ai đó đã gửi post đến địa chỉ này',
                    'message_reply' => 'Ai đó đã gửi post đến địa chỉ này',
                    'comment_status' => 0,
                    'message_status' => 0
                ];
                $this->app_log->add($log_data);
                return;
                die();
            }
            $log_data = [
                'uid' => 0,
                'post_id' => $data->entry[0]->changes[0]->value->parent_id,
                'page_id' => explode('_',$data->entry[0]->changes[0]->value->post->id)[0],
                'comment_data' => $feedData, 
                'comment_reply' => 'Page chưa đăng ký hoặc post/photo chưa set nội dung hoặc cài đặt chức năng tự động trả lời',
                'message_reply' => 'Page chưa đăng ký hoặc post/photo chưa set nội dung hoặc cài đặt chức năng tự động trả lời',
                'comment_status' => 0,
                'message_status' => 0
            ];
            $this->app_log->add($log_data);
            return;
            die();
            if ($data->object == 'page' && $data->entry[0]->changes[0]->field == 'feed') {
                $comment_id = $data->entry[0]->changes[0]->value->comment_id;
                $url = $data->entry[0]->changes[0]->value->post->permalink_url;
                $url = str_replace('https://www.facebook.com/', '', $url);
                $noti_type = explode('/', $url)[1];
                $post_id = '';
                if($noti_type === 'photos'){
                    $post_id = explode('_',$data->entry[0]->changes[0]->value->post->id)[1];
                }else if ($noti_type === 'posts'){
                    $post_id = $data->entry[0]->changes[0]->value->post->id;
                }
                $post = $this->fb_post->get_post_with_fanpage($post_id);
                if(!count($post)){
                    $log_data = [
                        'uid' => 0,
                        'post_id' => $data->entry[0]->changes[0]->value->parent_id,
                        'page_id' => explode('_',$data->entry[0]->changes[0]->value->post->id)[0],
                        'comment_data' => $feedData, 
                        'comment_reply' => 'Page chưa đăng ký hoặc post/photo chưa set nội dung hoặc cài đặt chức năng tự động trả lời',
                        'message_reply' => 'Page chưa đăng ký hoặc post/photo chưa set nội dung hoặc cài đặt chức năng tự động trả lời',
                        'comment_status' => 0,
                        'message_status' => 0
                    ];
                    $this->app_log->add($log_data);
                    return;
                    die();
                }
                $comment_reply = $post[0]['comment_reply'];
                $message_reply = $post[0]['message_reply'];
                $access_token = $post[0]['page_token'];
                // Set nội dung trả lời bình luận
                $comment_respon_data = [
                    'access_token' => $access_token,
                    'message' => $comment_reply
                ];
                // Set nội dung trả lời tin nhắn
                $reply_respon_data = [
                    'access_token' => $access_token,
                    'message' => $message_reply
                ];
                $log_data = [
                    'uid' => $post[0]['uid'],
                    'post_id' => $post_id,
                    'page_id' => $post[0]['fanpage_id'],
                    'comment_data' => $feedData
                ];
                // trả lời bình luận
                $url = "https://graph.facebook.com/".$this->v."/".$comment_id."/comments";
                $res = CURL_REQUEST($url, 'POST', $comment_respon_data);
                $log_data['comment_reply'] = $res;
                $log_data['comment_status'] = (int) strpos($res, 'error') < 0;

                // Nhắn tin đến user
                $url = "https://graph.facebook.com/".$this->v."/".$comment_id."/private_replies";
                $res = CURL_REQUEST($url, 'POST', $reply_respon_data);
                $log_data['message_reply'] = $res;
                $log_data['message_status'] = (int) strpos($res, 'error') < 0;

                // Ghi log
                $this->app_log->add($log_data);
                return;
                die;
            }
        }

        http_response_code(200);
    }

    public function view_page_data(){
        if (!count($this->userInfo)) {
            send_json([
                'status' => 0,
                'msg' => 'Bạn chưa đăng nhập'
            ]);
        }
        $account_info = $this->fb_account->get_row(['uid' => $this->userInfo['uid']]);
        if(empty($account_info)){
            send_json([
                'status' => 0,
                'msg' => 'Tài khoản chưa được kết nối'
            ]);
        }
        $data = $this->fanpage_account->get_all();
        send_json($data);
    }

    public function view_log(){
        if (!count($this->userInfo)) {
            send_json([
                'status' => 0,
                'msg' => 'Bạn chưa đăng nhập'
            ]);
        }
        $account_info = $this->fb_account->get_row(['uid > 0']);
        if(empty($account_info)){
            send_json([
                'status' => 0,
                'msg' => 'Tài khoản chưa được kết nối'
            ]);
        }
        $data = $this->app_log->filter(['uid' => $this->userInfo['uid']]);
        foreach ($data['data'] as &$d) {
            $d['comment_data'] = htmlspecialchars_decode($d['comment_data'],ENT_QUOTES);
            $d['comment_data'] = json_decode($d['comment_data']);
            $d['comment_reply'] = htmlspecialchars_decode($d['comment_reply'],ENT_QUOTES);
            $d['comment_reply'] = json_decode($d['comment_reply']);
            $d['message_reply'] = htmlspecialchars_decode($d['message_reply'],ENT_QUOTES);
            $d['message_reply'] = json_decode($d['message_reply']);
        }
        send_json($data);
    }

    /**
     * Kết nối tài khoản facebook với hệ thống
     */
    public function set_user_account(){
        $token = input('token', 'p', '');
        $fb_id = input('fb_id', 'p', '');
        $fb_token = input('access_token', 'p', '');
        if (!count($this->userInfo)) {
            send_json([
                'status' => 0,
                'msg' => 'Bạn chưa đăng nhập'
            ]);
        }
        if(empty($token)){
            send_json([
                'status' => 0,
                'msg' => 'Thiếu token'
            ]);
        }
        if ($token !== $this->session->get_session('main-token')) {
            send_json([
                'status' => 0,
                'msg' => 'Token không hợp lệ'
            ]);
        }
        if(empty($fb_id)){
            send_json([
                'status' => 0,
                'msg' => 'Thiếu facebook id'
            ]);
        }
        if(!$this->check_valid_fb_id($fb_id)){
            send_json([
                'status' => 0,
                'msg' => 'Facebook id không hợp lệ'
            ]);
        }
        if(empty($fb_token)){
            send_json([
                'status' => 0,
                'msg' => 'Thiếu facebook user token'
            ]);
        }
        $account_info = $this->fb_account->get_row(['uid' => $this->userInfo['uid']]);
        if(!empty($account_info)){
            send_json([
                'status' => 0,
                'msg' => 'Tài khoản đã được kết nối'
            ]);
        }
        $res = json_decode($this->get_long_lived_token($fb_token));
        if(isset($res->access_token) && !empty($res->access_token)){
            $expires_in = (int) $res->expires_in;
            $expires_in = ceil($expires_in/84600);
            $expires_to = new DateTime();
            $expires_to->add(new DateInterval('P'.$expires_in.'D'));
            $new_id = $this->fb_account->add([
                'uid' => $this->userInfo['uid'],
                'facebook_id' => $fb_id,
                'user_token' => $res->access_token,
                'expires_to' => $expires_to->format('Y-m-d H:i:s')
            ]);
            if($new_id){
                send_json([
                    'status' => 1,
                    'msg' => 'Kết nối tài khoản thành công'
                ]);
            }else{
                send_json([
                    'status' => 0,
                    'msg' => 'Thêm dữ liệu thất bại'
                ]);
            }
        }else{
            send_json([
                'status' => 0,
                'msg' => 'Không lấy được token'
            ]);
        }
    }

    /**
     * Xác nhận tài khoản facebook với hệ thống
     */
    public function verify_token(){
        $token = input('token', 'p', '');
        $fb_id = input('fb_id', 'p', '');
        $fb_token = input('access_token', 'p', '');
        if (!count($this->userInfo)) {
            send_json([
                'status' => 0,
                'msg' => 'Bạn chưa đăng nhập'
            ]);
        }
        if(empty($token)){
            send_json([
                'status' => 0,
                'msg' => 'Thiếu token'
            ]);
        }
        if ($token !== $this->session->get_session('verify-token')) {
            send_json([
                'status' => 0,
                'msg' => 'Token không hợp lệ'
            ]);
        }
        if(empty($fb_id)){
            send_json([
                'status' => 0,
                'msg' => 'Thiếu facebook id'
            ]);
        }
        if(!$this->check_valid_fb_id($fb_id)){
            send_json([
                'status' => 0,
                'msg' => 'Facebook id không hợp lệ'
            ]);
        }
        if(empty($fb_token)){
            send_json([
                'status' => 0,
                'msg' => 'Thiếu facebook user token'
            ]);
        }
        $account_info = $this->fb_account->get_row(['uid' => $this->userInfo['uid']]);
        if(empty($account_info)){
            send_json([
                'status' => 0,
                'msg' => 'Tài khoản chưa được kết nối'
            ]);
        }
        $new_token = json_decode($this->get_long_lived_token($fb_token));
        if(isset($new_token->access_token) && !empty($new_token->access_token)){
            $expires_in = (int) $new_token->expires_in;
            $expires_in = ceil($expires_in/84600);
            $expires_to = new DateTime();
            $expires_to->add(new DateInterval('P'.$expires_in.'D'));
            $id = $this->fb_account->update([
                'user_token' => $new_token->access_token,
                'expires_to' => $expires_to->format('Y-m-d H:i:s')
            ], [
                'uid' => $this->userInfo['uid'],
                'facebook_id' => $fb_id,
            ]);
            if($id){
                $url = 'https://graph.facebook.com/'.$this->v.'/'.$fb_id.'?fields=accounts&access_token='.$new_token->access_token. '&limit=50';
                $pages = json_decode(CURL_REQUEST($url, 'GET'));
                if( isset($pages->accounts->data) && is_array($pages->accounts->data) ){
                    foreach ($pages->accounts->data as $p) {
                        $this->fanpage_account->update([
                            'fanpage_name' => $p->name,
                            'page_token' => $p->access_token,
                            'category' => $p->category,
                            'perms' => implode(',', $p->perms)
                        ], [
                            'uid' => $this->userInfo['uid'],
                            'fanpage_id' => $p->id
                        ]);
                    }
                }
                $pages = $this->fanpage_account->filter([
                    'uid' => $this->userInfo['uid'], 
                    'is_subscribed' => 1,

                ]);
                foreach ($pages['data'] as $p) {
                    CURL_REQUEST(
                        'https://graph.facebook.com/'.$this->v.'/'.$p['fanpage_id'].'/subscribed_apps?access_token='.$p['page_token'], 
                        'POST', 
                        [
                            'access_token' => $p['page_token']
                        ]
                    );
                }
                send_json([
                    'status' => 1,
                    'msg' => 'Xác nhận tài khoản thành công'
                ]);
            }else{
                send_json([
                    'status' => 0,
                    'msg' => 'Xác nhận tài khoản thất bại'
                ]);
            }
        }else{
            send_json([
                'status' => 0,
                'msg' => 'Không lấy được token từ facebook'
            ]);
        }
    }

    /**
     * Lấy thông tin về account facebook của tài khoản hiện tại
     */
    public function get_facebook_account($token){
        if (!count($this->userInfo)) {
            send_json([
                'status' => 0,
                'msg' => 'Bạn chưa đăng nhập'
            ]);
        }
        if(empty($token)){
            send_json([
                'status' => 0,
                'msg' => 'Thiếu token'
            ]);
        }
        if ($token !== $this->session->get_session('main-token')) {
            send_json([
                'status' => 0,
                'msg' => 'Token không hợp lệ'
            ]);
        }
        
        $account_info = $this->fb_account->get_row(['uid' => $this->userInfo['uid']]);
        if(!empty($account_info)){
            send_json([
                'status' => 1,
                'msg' => 'Tài khoản đã được kết nối',
                'is_connected' => true
            ]);
        }else{
            send_json([
                'status' => 0,
                'msg' => 'Tài khoản chưa được kết nối',
                'is_connected' => false
            ]);
        }
    }

    /**
     * Lấy danh sách fanpage đăng ký nhận webhook
     */
    public function get_subscribed_page($token){
        if (!count($this->userInfo)) {
            send_json([
                'status' => 0,
                'msg' => 'Bạn chưa đăng nhập'
            ]);
        }
        if(empty($token)){
            send_json([
                'status' => 0,
                'msg' => 'Thiếu token'
            ]);
        }
        if ($token !== $this->session->get_session('content-token')) {
            send_json([
                'status' => 0,
                'msg' => 'Token không hợp lệ'
            ]);
        }
        $account_info = $this->fb_account->get_row(['uid' => $this->userInfo['uid']]);
        if(empty($account_info)){
            send_json([
                'status' => 0,
                'msg' => 'Tài khoản chưa được kết nối',
                'is_connected' => false
            ]);
        }else{
            $pages = $this->fanpage_account->filter(['uid' => $this->userInfo['uid'],'is_subscribed' => 1]);
            foreach ($pages['data'] as &$p) {
                $p = [
                    'category' => $p['category'],
                    'fanpage_id' => $p['fanpage_id'],
                    'fanpage_name' => $p['fanpage_name'],
                    'is_subscribed' => $p['is_subscribed']
                ];
            }
            send_json([
                'status' => 1,
                'msg' => 'success',
                'data' => $pages,
                'is_connected' => true
            ]);
        }
    }

    /**
     * Lấy danh sách bài post của fanpage
     */
    public function get_page_posts($page_id, $token){
        if (!count($this->userInfo)) {
            send_json([
                'status' => 0,
                'msg' => 'Bạn chưa đăng nhập'
            ]);
        }
        if(empty($token)){
            send_json([
                'status' => 0,
                'msg' => 'Thiếu token'
            ]);
        }
        if(empty($page_id)){
            send_json([
                'status' => 0,
                'msg' => 'Thiếu fanpage id'
            ]);
        }
        if(!$this->check_valid_fb_id($page_id)){
            send_json([
                'status' => 0,
                'msg' => 'Page id không hợp lệ'
            ]);
        }
        if ($token !== $this->session->get_session('content-token')) {
            send_json([
                'status' => 0,
                'msg' => 'Token không hợp lệ'
            ]);
        }
        $account_info = $this->fb_account->get_row(['uid' => $this->userInfo['uid']]);
        if(empty($account_info)){
            send_json([
                'status' => 0,
                'msg' => 'Tài khoản chưa được kết nối',
                'is_connected' => false
            ]);
        }else{
            $pages = $this->fanpage_account->get_row([
                'uid' => $this->userInfo['uid'], 
                'fanpage_id' => $page_id,
                'is_subscribed' => 1
            ]);
            if (!empty($pages) && is_array($pages)) {
                $url = 'https://graph.facebook.com/'.$this->v.'/'.$page_id.'/feed?fields=id,message,story,picture&limit=100&access_token='.$pages['page_token'];
                $res = CURL_REQUEST($url, 'GET');
                send_json([
                    'status' => 1,
                    'msg' => 'success',
                    'data' => json_decode($res)
                ]);
            }else{
                send_json([
                    'status' => 0,
                    'msg' => 'Fanpage không tồn tại hoặc chưa được kích hoạt'
                ]);
            }
        }
    }

    /**
     * Lấy danh sách ảnh của fanpage
     */
    public function get_page_photos($page_id, $token){
        if (!count($this->userInfo)) {
            send_json([
                'status' => 0,
                'msg' => 'Bạn chưa đăng nhập'
            ]);
        }
        if(empty($token)){
            send_json([
                'status' => 0,
                'msg' => 'Thiếu token'
            ]);
        }
        if(empty($page_id)){
            send_json([
                'status' => 0,
                'msg' => 'Thiếu fanpage id'
            ]);
        }
        if(!$this->check_valid_fb_id($page_id)){
            send_json([
                'status' => 0,
                'msg' => 'Page id không hợp lệ'
            ]);
        }
        if ($token !== $this->session->get_session('content-token')) {
            send_json([
                'status' => 0,
                'msg' => 'Token không hợp lệ'
            ]);
        }
        $account_info = $this->fb_account->get_row(['uid' => $this->userInfo['uid']]);
        if(empty($account_info)){
            send_json([
                'status' => 0,
                'msg' => 'Tài khoản chưa được kết nối',
                'is_connected' => false
            ]);
        }else{
            $pages = $this->fanpage_account->get_row([
                'uid' => $this->userInfo['uid'], 
                'fanpage_id' => $page_id,
                'is_subscribed' => 1
            ]);
            if (!empty($pages) && is_array($pages)) {
                $url = 'https://graph.facebook.com/'.$this->v.'/'.$page_id.'/photos?fields=id,name,picture&type=uploaded&limit=100&access_token='.$pages['page_token'];
                $res = CURL_REQUEST($url, 'GET');
                send_json([
                    'status' => 1,
                    'msg' => 'success',
                    'data' => json_decode($res)
                ]);
            }else{
                send_json([
                    'status' => 0,
                    'msg' => 'Fanpage không tồn tại hoặc chưa được kích hoạt'
                ]);
            }
        }
    }

    /**
     * Lấy danh sách bài post đã đăng ký nhận webhook
     */
    public function get_subscribed_posts($token, $page = 1){
        if (!count($this->userInfo)) {
            send_json([
                'status' => 0,
                'msg' => 'Bạn chưa đăng nhập'
            ]);
        }
        if(empty($token)){
            send_json([
                'status' => 0,
                'msg' => 'Thiếu token'
            ]);
        }
        if ($token !== $this->session->get_session('content-token')) {
            send_json([
                'status' => 0,
                'msg' => 'Token không hợp lệ'
            ]);
        }
        $account_info = $this->fb_account->get_row(['uid' => $this->userInfo['uid']]);
        if(empty($account_info)){
            send_json([
                'status' => 0,
                'msg' => 'Tài khoản chưa được kết nối',
                'is_connected' => false
            ]);
        }else{
            $posts = $this->fb_post->get_all_availables($this->userInfo['uid']);
            send_json([
                'status' => 1,
                'msg' => 'success',
                'data' => $posts
            ]);
        }
    }

    /**
     * Đăng ký nhận webhook cho bài post hoặc ảnh 
     */
    public function add_subscribed_posts(){
        $posts = input('posts', 'p', '');
        $photos = input('photos', 'p', '');
        $fanpage_id = input('fanpage_id', 'p', '');
        $comment_reply = input('comment_reply', 'p', '');
        $message_reply = input('message_reply', 'p', '');
        $token = input('token', 'p', '');
        if (!count($this->userInfo)) {
            send_json([
                'status' => 0,
                'msg' => 'Bạn chưa đăng nhập'
            ]);
        }
        if(empty($token)){
            send_json([
                'status' => 0,
                'msg' => 'Thiếu token'
            ]);
        }
        if ($token !== $this->session->get_session('content-token')) {
            send_json([
                'status' => 0,
                'msg' => 'Token không hợp lệ'
            ]);
        }
        $account_info = $this->fb_account->get_row(['uid' => $this->userInfo['uid']]);
        if(empty($account_info)){
            send_json([
                'status' => 0,
                'msg' => 'Tài khoản chưa được kết nối',
                'is_connected' => false
            ]);
        }
        if(empty($posts) && empty($photos)){
            send_json([
                'status' => 0,
                'msg' => 'Không có bài post và ảnh nào!'
            ]);
        }
        if(empty($fanpage_id)){
            send_json([
                'status' => 0,
                'msg' => 'Thiếu page id'
            ]);
        }
        if(!$this->check_valid_fb_id($fanpage_id)){
            send_json([
                'status' => 0,
                'msg' => 'Page id không hợp lệ'
            ]);
        }
        if(empty($comment_reply)){
            send_json([
                'status' => 0,
                'msg' => 'Thiếu nội dung trả lời bình luận'
            ]);
        }
        if(empty($message_reply)){
            send_json([
                'status' => 0,
                'msg' => 'Thiếu nội dung tin nhắn trả lời'
            ]);
        }
        $pages = $this->fanpage_account->get_row([
            'uid' => $this->userInfo['uid'], 
            'fanpage_id' => $fanpage_id,
            'is_subscribed' => 1
        ]);
        if(empty($pages) || !is_array($pages)) {
            send_json([
                'status' => 0,
                'msg' => 'Fanpage không tồn tại hoặc chưa được kích hoạt'
            ]);
        }
        $count_posts_added = 0;
        $count_photos_added = 0;
        if(count($posts)){
            foreach ($posts as &$post) {
                $post = [
                    'post_id'       => $post['id'],
                    'parent_id'     => $fanpage_id,
                    'post_content'  => empty($post['message']) ? $post['story'] : $post['message'],
                    'post_thumb'    => isset($post['picture']) ? $post['picture'] : '',
                    'comment_reply' => $comment_reply,
                    'message_reply' => $message_reply,
                    'is_running'    => 1
                ];
                $post_id = $this->fb_post->add($post);
                if ($post_id) {
                    $post['id'] = en_id($post_id);
                    $count_posts_added++;
                }else{
                    unset($post);
                }
            }
        }
        if(count($photos)){
            foreach ($photos as &$photo) {
                $photo = [
                    'post_id'       => $photo['id'],
                    'parent_id'     => $fanpage_id,
                    'post_content'  => isset($photo['name']) ? $photo['name'] : '',
                    'post_thumb'    => $photo['picture'],
                    'comment_reply' => $comment_reply,
                    'message_reply' => $message_reply,
                    'is_running'    => 1
                ];
                $photo_id = $this->fb_post->add($photo);
                if ($photo_id) {
                    $photo['id'] = en_id($photo_id);
                    $count_photos_added++;
                }else{
                    unset($photo);
                }
            }
        }
        if($count_posts_added || $count_photos_added){
            send_json([
                'status' => 1,
                'msg' => 'Thêm thành công!<br>' . $count_posts_added . ' bài post.<br>'.$count_photos_added.' ảnh',
                'posts' => array_merge( (array) $posts, (array) $photos)
            ]);
        }else{
            send_json([
                'status' => 0,
                'msg' => 'Chưa thêm được hoặc bài post hoặc ảnh đã được thêm trước đó.<br>Vui lòng xem lại!'
            ]);
        }
    }

    /**
     * Cập nhật nội dung trả lời bài post
     */
    public function update_subscribed_posts(){
        $post_id = input('post_id', 'p', '');
        $parent_id = input('parent_id', 'p', '');
        $comment_reply = input('comment_reply', 'p', '');
        $message_reply = input('message_reply', 'p', '');
        $is_running = (int) input('is_running', 'p', -1);
        $token = input('token', 'p', '');
        if (!count($this->userInfo)) {
            send_json([
                'status' => 0,
                'msg' => 'Bạn chưa đăng nhập'
            ]);
        }
        if(empty($token)){
            send_json([
                'status' => 0,
                'msg' => 'Thiếu token'
            ]);
        }
        if ($token !== $this->session->get_session('content-token')) {
            send_json([
                'status' => 0,
                'msg' => 'Token không hợp lệ'
            ]);
        }
        $account_info = $this->fb_account->get_row(['uid' => $this->userInfo['uid']]);
        if(empty($account_info)){
            send_json([
                'status' => 0,
                'msg' => 'Tài khoản chưa được kết nối',
                'is_connected' => false
            ]);
        }
        if(empty($post_id)){
            send_json([
                'status' => 0,
                'msg' => 'Thiếu post id'
            ]);
        }
        if(!$this->check_valid_fb_id($post_id)){
            send_json([
                'status' => 0,
                'msg' => 'Post id không hợp lệ'
            ]);
        }
        if(empty($parent_id)){
            send_json([
                'status' => 0,
                'msg' => 'Thiếu page id'
            ]);
        }
        if(!$this->check_valid_fb_id($parent_id)){
            send_json([
                'status' => 0,
                'msg' => 'Page id không hợp lệ'
            ]);
        }
        if(empty($comment_reply)){
            send_json([
                'status' => 0,
                'msg' => 'Thiếu nội dung trả lời bình luận'
            ]);
        }
        if(empty($message_reply)){
            send_json([
                'status' => 0,
                'msg' => 'Thiếu nội dung tin nhắn trả lời'
            ]);
        }
        if($is_running < 0 || $is_running > 1){
            send_json([
                'status' => 0,
                'msg' => 'Trạng thái sai định dạng'
            ]);
        }
        $pages = $this->fanpage_account->get_row([
            'uid' => $this->userInfo['uid'], 
            'fanpage_id' => $parent_id,
            'is_subscribed' => 1
        ]);
        if(empty($pages) || !is_array($pages)) {
            send_json([
                'status' => 0,
                'msg' => 'Fanpage không tồn tại hoặc chưa được kích hoạt'
            ]);
        }
        $post_id = $this->fb_post->update([
            'comment_reply' => $comment_reply,
            'message_reply' => $message_reply,
            'is_running'    => $is_running
        ], [
            'post_id'       => $post_id,
            'parent_id'     => $parent_id
        ]);
        if($post_id){
            send_json([
                'status' => 1,
                'msg' => 'Cập nhật thành công'
            ]);
        }else{
            send_json([
                'status' => 0,
                'msg' => 'Cập nhật thất bại'
            ]);
        }
    }

    /**
     * Xóa bài đã đăng ký
     */
    public function delete_subscribed_posts(){
        $post_id = input('post_id', 'p', '');
        $token = input('token', 'p', '');
        if (!count($this->userInfo)) {
            send_json([
                'status' => 0,
                'msg' => 'Bạn chưa đăng nhập'
            ]);
        }
        if(empty($token)){
            send_json([
                'status' => 0,
                'msg' => 'Thiếu token'
            ]);
        }
        if ($token !== $this->session->get_session('content-token')) {
            send_json([
                'status' => 0,
                'msg' => 'Token không hợp lệ'
            ]);
        }
        $account_info = $this->fb_account->get_row(['uid' => $this->userInfo['uid']]);
        if(empty($account_info)){
            send_json([
                'status' => 0,
                'msg' => 'Tài khoản chưa được kết nối',
                'is_connected' => false
            ]);
        }
        if(empty($post_id)){
            send_json([
                'status' => 0,
                'msg' => 'Thiếu post id'
            ]);
        }
        if(!$this->check_valid_fb_id($post_id)){
            send_json([
                'status' => 0,
                'msg' => 'Post id không hợp lệ'
            ]);
        }
        $post_id = $this->fb_post->delete(['post_id' => $post_id]);
        if($post_id){
            send_json([
                'status' => 1,
                'msg' => 'Xóa thành công'
            ]);
        }else{
            send_json([
                'status' => 0,
                'msg' => 'Xóa thất bại'
            ]);
        }
    }

}
