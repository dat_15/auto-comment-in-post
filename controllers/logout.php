<?php
/**
* index controller.
*/
class logout extends Controller
{
    private $session;

    public function __construct()
    {
        parent::__construct(__CLASS__);
        $this->session = new Session();
        $callbackURL = $this->session->get_session('goToUrl');
        $this->session->delete_session('isLoggedIn');
        $this->session->delete_session('uid');
        $this->session->delete_session('email');
        $this->session->set_session('goToUrl', $callbackURL);
        header('Location: '.USER_LOGIN);
    }

    public function index()
    {
    }
}
