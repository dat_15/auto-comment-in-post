<?php

class home extends Controller
{
    private $model;

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->load_view('common/header', ['title' => 'Trang chủ', 'page' => 'index']);
        $this->load_view('common/index');
        $this->load_view('common/footer');
    }
}
