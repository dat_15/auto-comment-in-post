<header id="header">
	<div class="header-sidebar hidden-xs">
		Kite Admin
	</div>
	<div class="menu">
		<nav>
			<div class="nav-left pull-left">
				<label for="toggle-menu" class="toggle-menu-btn">
					<span></span>
					<span></span>
					<span></span>
				</label>
				<form action="" method="get" accept-charset="utf-8">
					<input type="" name="" class="form-control" placeholder="Search">
					<span class="glyphicon glyphicon-search"></span>
				</form>
			</div>
			<div class="nav-right pull-right text-right">
				<ul>
					<li>
						<label for="sub-message-checkbox" class="active">
							Messages
						</label>
						<input type="checkbox" class="hidden" id="sub-message-checkbox">
						<ul class="sub-menu">
							<li class="clearfix">
								<a href="javascript:void(0)">
									<div class="msg-avatar col-xs-2">
										<img src="assets/img/user_2.jpg" alt="Jane Roe" class="img-circle">
									</div>
									<div class="msg-content col-xs-10">
										<div class="msg-content-header clearfix">
											<strong class="pull-left">
												Jane Roe
											</strong>
											<span class="pull-right">
												2 hours ago
											</span>
										</div>
										<div class="msg-content-text text-left">
											Lorem ipsum dolor sit amet, elit ...
										</div>
									</div>
								</a>
							</li>
							<li class="clearfix">
								<a href="javascript:void(0)">
									<div class="msg-avatar col-xs-2">
										<img src="assets/img/user_3.jpg" alt="John Doe" class="img-circle">
									</div>
									<div class="msg-content col-xs-10">
										<div class="msg-content-header clearfix">
											<strong class="pull-left">
												John Doe
											</strong>
											<span class="pull-right">
												1 day ago
											</span>
										</div>
										<div class="msg-content-text text-left">
											Lorem ipsum dolor sit amet, elit ...
										</div>
									</div>
								</a>
							</li>
							<li class="clearfix">
								<a href="javascript:void(0)">
									<div class="msg-avatar col-xs-2">
										<img src="assets/img/user_4.jpg" alt="Mary Major" class="img-circle">
									</div>
									<div class="msg-content col-xs-10">
										<div class="msg-content-header clearfix">
											<strong class="pull-left">
												Mary Major
											</strong>
											<span class="pull-right">
												1 day ago
											</span>
										</div>
										<div class="msg-content-text text-left">
											Lorem ipsum dolor sit amet, elit ...
										</div>
									</div>
								</a>
							</li>
							<li class="clearfix">
								<a href="javascript:void(0)" class="text-center">
									VIEW ALL
								</a>
							</li>
						</ul>
					</li>
					<li>
						<label for="sub-alert-checkbox">
							Alert
						</label>
						<input type="checkbox" id="sub-alert-checkbox" class="hidden">
						<ul class="sub-menu sub-alert-content">
							<li>
								<a href="javascript:void(0);" class="text-left">
									<div class="col-xs-2">
										<span class="label img-circle label-success">
											<i class="fa fa-user"></i>
										</span>
									</div>
									<div class="col-xs-10">
										<span class="text-left text-muted">
											New user registerd
										</span>
										<span class="pull-right">
											3 mins ago
										</span>
									</div>
								</a>
							</li>
							<li>
								<a href="javascript:void(0);" class="text-left">
									<div class="col-xs-2">
										<span class="label img-circle label-danger">
											<i class="fa fa-flash"></i>
										</span>
									</div>
									<div class="col-xs-10">
										<span class="text-left text-muted">
											Server overloaded
										</span>
										<span class="pull-right">
											15 mins ago
										</span>
									</div>
								</a>
							</li>
							<li>
								<a href="javascript:void(0);" class="text-left">
									<div class="col-xs-2">
										<span class="label img-circle label-warning">
											<i class="fa fa-bell"></i>
										</span>
									</div>
									<div class="col-xs-10">
										Server not responding
										<span class="pull-right">
											1 hour ago
										</span>
									</div>
								</a>
							</li>
							<li>
								<a href="javascript:void(0);" class="text-left">
									<div class="col-xs-2">
										<span class="label img-circle label-success">
											<i class="fa fa-user"></i>
										</span>
									</div>
									<div class="col-xs-10">
										<span class="text-left text-muted">
											New report
										</span>
										<span class="pull-right">
											2 hours ago
										</span>
									</div>
								</a>
							</li>
							<li>
								<a href="javascript:void(0);" class="text-left">
									<div class="col-xs-2">
										<span class="label img-circle label-danger">
											<i class="fa fa-flash"></i>
										</span>
									</div>
									<div class="col-xs-10">
										<span class="text-left text-muted">
											Database error
										</span>
										<span class="pull-right">
											1 day ago
										</span>
									</div>
								</a>
							</li>
							<li class="clearfix">
								<a href="javascript:void(0)" class="text-center">
									VIEW ALL
								</a>
							</li>
						</ul>
					</li>
					<li>
						<button class="btn btn-warning text-uppercase">
							sign out
						</button>
					</li>
				</ul>
			</div>
		</nav>
	</div>
</header>