<link rel="stylesheet" type="text/css" href="assets/css/page-1.css?v=<?= time();?>">
<div class="main-content">
	<div class="col-xs-12">
		<h3 class="title">
			Dashboard 
			<small>
				Dashboard and statistics
			</small>
		</h3>
	</div>

	<div class="col-xs-12 mt-20">
		<div id="dashboard-statistics" class="mt--7-5">
			<div class="col-xs-12 col-sm-4 col-md-3">
				<div class="label label-danger">
					<i class="fa fa-comments"></i>
					<h1 class="text-right">1250</h1>
					<span class="pull-right">Comments</span>
				</div>
			</div>
			<div class="col-xs-12 col-sm-4 col-md-3">
				<div class="label label-warning">
					<i class="fa fa-globe"></i>
					<h1 class="text-right">5412</h1>
					<span class="pull-right">Customers</span>
				</div>
			</div>
			<div class="col-xs-12 col-sm-4 col-md-3">
				<div class="label label-success">
					<i class="fa fa-pie-chart"></i>
					<h1 class="text-right">4155</h1>
					<span class="pull-right">Orders</span>
				</div>
			</div>
			<div class="col-xs-12 col-sm-4 col-md-3">
				<div class="label label-info">
					<i class="fa fa-eur"></i>
					<h1 class="text-right">$105K</h1>
					<span class="pull-right">Totol profit</span>
				</div>
			</div>
		</div>
	</div>
	<div class="col-xs-12 mt-20">
		<div class="mt--7-5">
			<div class="col-xs-12 col-sm-6 col-md-6">
				<div class="empty-content">
					<h4>Unique visitors</h4>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6">
				<div class="empty-content">
					<h4>Revenue </h4>
				</div>
			</div>
		</div>
	</div>
	<div class="col-xs-12 mt-20">
		<div id="activities" class="mt--7-5">
			<div class="col-xs-12 col-sm-6 col-md-6">
				<div class="activities-content">
					<h4>Lastest comments</h4>
					<div class="media">
						<div class="media-left">
							<a href="#">
								<img class="media-object" src="assets/img/user_1.jpg" alt="John Doe">
							</a>
						</div>
						<div class="media-body">
							<span class="media-heading h4">
								<strong>John Doe</strong>
								<small>2 hours ago</small>
							</span>
							<p class="text-muted">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum, corporis. Voluptatibus odio perspiciatis non quisquam provident, quasi eaque officia.
							</p>
							<div class="media media-child">
								<a href="" class="media-action">view</a>
								<a href="" class="media-action">edit</a>
								<a href="" class="media-action">remove</a>
							</div>
						</div>
					</div>
					<div class="media">
						<div class="media-left">
							<a href="#">
								<img class="media-object" src="assets/img/user_2.jpg" alt="Jane Doe">
							</a>
						</div>
						<div class="media-body">
							<span class="media-heading h4">
								<strong>Jane Doe</strong>
								<small>2 hours ago</small>
							</span>
							<p class="text-muted">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum, corporis. Voluptatibus odio perspiciatis non quisquam provident, quasi eaque officia.
							</p>
							<div class="media media-child">
								<a href="" class="media-action">view</a>
								<a href="" class="media-action">edit</a>
								<a href="" class="media-action">remove</a>
							</div>
						</div>
					</div>
					<div class="media">
						<div class="media-left">
							<a href="#">
								<img class="media-object" src="assets/img/user_1.jpg" alt="John Doe">
							</a>
						</div>
						<div class="media-body">
							<span class="media-heading h4">
								<strong>John Doe</strong>
								<small>2 hours ago</small>
							</span>
							<p class="text-muted">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum, corporis. Voluptatibus odio perspiciatis non quisquam provident, quasi eaque officia.
							</p>
							<div class="media media-child">
								<a href="" class="media-action">view</a>
								<a href="" class="media-action">edit</a>
								<a href="" class="media-action">remove</a>
							</div>
						</div>
					</div>
					<div class="activities-footer">
						<button class="btn btn-primary text-uppercase">
							view all comments
						</button>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6">
				<div class="activities-content">
					<h4>Daily feed</h4>
					<ul class="feed-content">
						<li>
							<a href="javascript:void(0);" class="text-left">
								<div class="col-xs-2">
									<span class="label img-circle label-success">
										<i class="fa fa-user"></i>
									</span>
								</div>
								<div class="col-xs-10">
									<span class="text-left text-muted">
										New user registerd
									</span>
									<span class="pull-right">
										3 mins ago
									</span>
								</div>
							</a>
						</li>
						<li>
							<a href="javascript:void(0);" class="text-left">
								<div class="col-xs-2">
									<span class="label img-circle label-danger">
										<i class="fa fa-flash"></i>
									</span>
								</div>
								<div class="col-xs-10">
									<span class="text-left text-muted">
										Server overloaded
									</span>
									<span class="pull-right">
										15 mins ago
									</span>
								</div>
							</a>
						</li>
						<li>
							<a href="javascript:void(0);" class="text-left">
								<div class="col-xs-2">
									<span class="label img-circle label-warning">
										<i class="fa fa-bell"></i>
									</span>
								</div>
								<div class="col-xs-10">
									<span class="pull-left text-muted">
										Server not responding
									</span>
									<span class="pull-right">
										1 hour ago
									</span>
								</div>
							</a>
						</li>
						<li>
							<a href="javascript:void(0);" class="text-left">
								<div class="col-xs-2">
									<span class="label img-circle label-success">
										<i class="fa fa-user"></i>
									</span>
								</div>
								<div class="col-xs-10">
									<span class="text-left text-muted">
										New report
									</span>
									<span class="pull-right">
										2 hours ago
									</span>
								</div>
							</a>
						</li>
						<li>
							<a href="javascript:void(0);" class="text-left">
								<div class="col-xs-2">
									<span class="label img-circle label-danger">
										<i class="fa fa-flash"></i>
									</span>
								</div>
								<div class="col-xs-10">
									<span class="text-left text-muted">
										Database error
									</span>
									<span class="pull-right">
										1 day ago
									</span>
								</div>
							</a>
						</li>
					</ul>
					<div class="activities-footer">
						<button class="btn btn-primary text-uppercase">
							view all notifications
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	window.onload = function(){
		var ativities_content = document.getElementsByClassName('activities-content');
		var max_height = Math.max(ativities_content[0].clientHeight, ativities_content[1].clientHeight);
		if (max_height > 500) {
			max_height = 500;
		}
		ativities_content[0].style.height = (max_height+60)+'px';
		ativities_content[1].style.height = (max_height+60)+'px';
	};
	window.onresize = function(event) {
		var ativities_content = document.getElementsByClassName('activities-content');
		var max_height = Math.max(ativities_content[0].clientHeight, ativities_content[1].clientHeight);
		if (max_height > 500) {
			max_height = 500;
		}
		ativities_content[0].style.height = (max_height+60)+'px';
		ativities_content[1].style.height = (max_height+60)+'px';
	};
</script>