<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jointjs/2.1.0/joint.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/3.10.1/lodash.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/backbone.js/1.3.3/backbone.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jointjs/2.1.0/joint.js"></script>
<div class="main-content">
	<div class="col-xs-12">
		<h3 class="title">
			JointJS 
			<small>
				Making amazing flowchart
			</small>
		</h3>
	</div>
	<div class="col-xs-12 mt-20">
		<div class="mt--7-5">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="">
					<h4>Demo for symper</h4>
					<div id="myholder"></div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

    var graph = new joint.dia.Graph;

    var paper = new joint.dia.Paper({
        el: document.getElementById('myholder'),
        model: graph,
        width: '100%',
        height: 650,
        gridSize: 10,
        drawGrid: true,
        background: {
            color: 'rgb(255, 255, 255)'
        }
    });

    var portTop = {
	    group: 'a'
	};

    var portRight = {
	    group: 'd'
	};
	
    var portBottom = {
	    group: 'c'
	};

    var portLeft = {
	    group: 'b'
	};

    var rect = new joint.shapes.standard.Rectangle({
    	posts: {
    		groups: {
    			'a': {
    				position: { 
    					name: 'top',
    					args: {
					        x: 10,
					        y: 10,
					        angle: 30,
					        dx: 1,
					        dy: 1
					    }
	    			}
    			},
    			'b': {
    				position: { 
    					name: 'left',
    					args: {
					        x: 10,
					        y: 10,
					        angle: 30,
					        dx: 1,
					        dy: 1
					    }
	    			}
    			},
    			'c': {
    				position: { 
    					name: 'bottom',
    					args: {
					        x: 10,
					        y: 10,
					        angle: 30,
					        dx: 1,
					        dy: 1
					    }
	    			}
    			},
    			'd': {
    				position: { 
    					name: 'right',
    					args: {
					        x: 10,
					        y: 10,
					        angle: 30,
					        dx: 1,
					        dy: 1
					    }
	    			}
    			}
    		}
    	}
    });
    rect.position(100, 30);
    rect.resize(130, 130);
    rect.addPort(portTop);
    rect.addPort(portRight);
    rect.addPort(portBottom);
    rect.addPort(portLeft);
    rect.attr({
        body: {
            fill: '#2196f3',
            rx: 5,
            ry: 5,
            strokeWidth: 2,
            stroke: '#ccc'
        },
        label: {
            text: 'Đề nghị nhập kho',
            fill: '#fff',
            fontSize: 18,
            fontWeight: 'bold'
        }
    });
    rect.addTo(graph);

    var circle = new joint.shapes.standard.Circle();
    circle.position(100, 250);
    circle.resize(130, 130);
    // circle.addPort(port);
    circle.attr({
        body: {
            fill: '#2196f3',
            strokeWidth: 2,
            stroke: '#ccc'
        },
        label: {
            text: 'Duyệt DNNK',
            fill: '#fff',
            fontSize: 18,
            fontWeight: 'bold'
        }
    });
    circle.addTo(graph);

    var rect2 = rect.clone();
    rect2.translate(250, 220);
    rect2.attr('label/text', 'Phiếu nhập kho');
    rect2.addTo(graph);

    var circle2 = circle.clone();
    circle2.translate(250, 220);
    circle2.attr('label/text', 'Duyệt phiếu nhập kho');
    circle2.addTo(graph);

    // var link = new joint.shapes.standard.Link();
    // link.attr('line/stroke', '#2196f3');
    // link.source(rect);
    // link.target(circle);
    // link.addTo(graph);
    // var link2 = link.clone();
    // link2.source(circle).target(rect2).addTo(graph);
    // var link3 = link.clone();
    // link2.source(rect2).target(circle2).addTo(graph);
</script>