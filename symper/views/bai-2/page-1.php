<link rel="stylesheet" type="text/css" href="assets/css/page-2.css?v=<?= time();?>">
<div class="main-content">
	<div class="col-xs-12">
		<h3 class="title">
			Danh sách lớp 
			<small>
				  Quản lý danh sách lớp
			</small>
		</h3>
	</div>
	<div class="col-xs-12 mt-20">
		<div class="mt--7-5">
			<div class="">
				<div>
					<!-- <h4>Basic initialization</h4> -->
					<table class="table">
						<thead>
              <th>Tên lớp</th>
							<th>Giáo viên phụ trách</th>
              <th>Số tín chỉ</th>
							<th>Khoa</th>
              <th>Danh sách sinh viên</th>
						</thead>
						<tbody>
              <?php foreach ($list['data'] as $lop): ?>
                <tr>
                  <td><?php echo $lop['ten_lop']; ?></td>
                  <td>
                    <a href="assignment-2?teacher=<?= $lop['teacher_id']?>" title="">
                      <?php echo $lop['ho_ten']; ?>
                    </a>
                  </td>
                  <td><?php echo $lop['so_tin_chi']; ?></td>
                  <td><?php echo $lop['khoa']; ?></td>
                  <td>
                    <a href="assignment-2/page-2?class=<?= $lop['class_id']?>" title="" class="btn btn-primary">
                      <i class="fa fa-eye" aria-hidden="true"></i>
                    </a>
                  </td>
                </tr>
              <?php endforeach ?>
            </tbody>
            <tfoot>
              <?php echo $pagination->getTemplate(); ?>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>