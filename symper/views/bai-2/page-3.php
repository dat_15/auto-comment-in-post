<link rel="stylesheet" type="text/css" href="assets/css/page-2.css?v=<?= time();?>">
<div class="main-content">
	<div class="col-xs-12">
		<h3 class="title">
			Danh sách giảng viên
			<small>
				  Quản lý danh sách giảng viên
			</small>
		</h3>
	</div>
	<div class="col-xs-12 mt-20">
		<div class="mt--7-5">
			<div class="">
				<div>
					<!-- <h4>Basic initialization</h4> -->
					<table class="table">
						<thead>
              <th>Họ tên</th>
              <th>Giới tính</th>
							<th>Email</th>
              <th>Số điện thoại</th>
              <th>Khoa</th>
              <th>Kinh nghiệm</th>
              <th>Danh sách lớp giảng dạy</th>
						</thead>
						<tbody>
              <?php foreach ($list['data'] as $teacher): ?>
                <tr>
                  <td><?php echo $teacher['ho_ten']; ?></td>
                  <td><?php echo $teacher['gender']&2 ? "Nữ" : 'Nam'; ?></td>
                  <td><?php echo $teacher['email']; ?></td>
                  <td><?php echo $teacher['phone']; ?></td>
                  <td><?php echo $teacher['khoa']; ?></td>
                  <td><?php echo $teacher['kinh_nghiem'] < 1 ? 'Chưa có' : $teacher['kinh_nghiem'] . ' năm'; ?></td>
                  <td>
                    <a href="assignment-2/?teacher=<?= $teacher['id']?>" title="" class="btn btn-primary">
                      <i class="fa fa-eye" aria-hidden="true"></i>
                    </a>
                  </td>
                </tr>
              <?php endforeach ?>
            </tbody>
            <tfoot>
              <?php echo $pagination->getTemplate(); ?>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>