<link rel="stylesheet" type="text/css" href="assets/css/page-2.css?v=<?= time();?>">
<div class="main-content">
	<div class="col-xs-12">
		<h3 class="title">
			Danh sách sinh viên
			<small>
				  Quản lý danh sách sinh viên
			</small>
		</h3>
	</div>
	<div class="col-xs-12 mt-20">
		<div class="mt--7-5">
			<div class="">
				<div>
					<!-- <h4>Basic initialization</h4> -->
					<table class="table">
						<thead>
              <th>Họ tên</th>
              <th>Giới tính</th>
							<th>Email</th>
              <th>Số điện thoại</th>
              <th>Ngày bắt đầu</th>
              <th>Danh sách lớp theo học</th>
						</thead>
						<tbody>
              <?php foreach ($list['data'] as $student): ?>
                <tr>
                  <td><?php echo $student['ho_ten']; ?></td>
                  <td><?php echo $student['gender']&2 ? "Nữ" : 'Nam'; ?></td>
                  <td><?php echo $student['email']; ?></td>
                  <td><?php echo $student['phone']; ?></td>
                  <td><?php echo $student['date']; ?></td>
                  <td>
                    <a href="assignment-2/?student=<?= $student['id']?>" title="" class="btn btn-primary">
                      <i class="fa fa-eye" aria-hidden="true"></i>
                    </a>
                  </td>
                </tr>
              <?php endforeach ?>
            </tbody>
            <tfoot>
              <?php echo $pagination->getTemplate(); ?>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>