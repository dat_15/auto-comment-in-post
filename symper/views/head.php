<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>
		<?php echo $title;?>
	</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
	<base href="<?= DOMAIN;?>">
	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap-theme.min.css?v=<?= time();?>">
	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css?v=<?= time();?>">
	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="assets/css/style.css?v=<?= time();?>">
	<link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i&amp;subset=vietnamese" rel="stylesheet">
</head>
<body>