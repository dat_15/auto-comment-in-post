<input type="checkbox" id="toggle-menu" class="hidden">
<div class="sidebar">
	<div class="sidebar-header">
		<div class="col-xs-4">
			<div class="avatar">
				<img src="assets/img/user_1.jpg" class="img-responsive" alt="Richard Roe">
			</div>
		</div>
		<div class="username col-xs-8">
			<strong>
				Richard Roe
			</strong>
			<br>
			<label class="text-muted" for="user-menu">
				Administrator 
				<span class="arrow-down"></span>
			</label>
		</div>
	</div>
	<div class="main-menu">
		<input type="checkbox" id="user-menu" class="hidden">
		<ul class="sidebar-submenu">
			<li>
				<a href="javascript:void(0);" title="">
					<i class="fa fa-user"></i> Profile
				</a>
			</li>
			<li>
				<a href="javascript:void(0);" title="">
					<i class="fa fa-edit"></i> Edit profile
				</a>
			</li>
			<li>
				<a href="javascript:void(0);" title="">
					<i class="fa fa-envelope"></i> Inbox
				</a>
			</li>
			<li>
				<a href="javascript:void(0);" title="">
					<i class="fa fa-sign-out"></i> Sign out
				</a>
			</li>
		</ul>
		<span class="menu-name">
			Symper test
		</span>
		<ul class="sidebar-menu">
			<li class="has-submenu">
				<label for="toggle-assignment-1">
					<i class="fa fa-book"></i> Assignment 1
				</label>
				<input type="checkbox" class="hidden" checked id="toggle-assignment-1">
				<ul class="sidebar-submenu">
					<li>
						<a href="home" title="">
							Page 1
						</a>
					</li>
					<li>
						<a href="home/page-2" title="">
							Page 2
						</a>
					</li>
				</ul>
			</li>
			<li class="has-submenu active">
				<label for="toggle-assignment-2">
					<i class="fa fa-book"></i> Assignment 2
				</label>
				<input type="checkbox" class="hidden" checked id="toggle-assignment-2">
				<ul class="sidebar-submenu">
					<li>
						<a href="assignment-2" title="">
							Danh sách lớp
						</a>
					</li>
					<li>
						<a href="assignment-2/page-2" title="">
							Danh sách sinh viên
						</a>
					</li>
					<li>
						<a href="assignment-2/page-3" title="">
							Danh sách giảng viên
						</a>
					</li>
				</ul>
			</li>
			<li class="">
				<a href="joint">
					<i class="fa fa-book"></i> Assignment 3
				</a>
			</li>
		</ul>
		<span class="menu-name">
			dashboards
		</span>
		<ul class="sidebar-menu">
			<li class="has-submenu">
				<label for="toggle-dashboard">
					
					<i class="fa fa-home"></i> Dashboards
				</label>
				<input type="checkbox" class="hidden" id="toggle-dashboard">
				<ul class="sidebar-submenu">
					<li>
						<a href="javascript:void(0);" title="">
							Dashboards 1
						</a>
					</li>
					<li>
						<a href="javascript:void(0);" title="">
							Dashboards 2
						</a>
					</li>
				</ul>
			</li>
		</ul>
		<span class="menu-name">
			Features
		</span>
		<ul class="sidebar-menu">
			<li class="has-submenu">
				<label for="toggle-forms">
					<i class="fa fa-check-square"></i> Forms
				</label>
				<input type="checkbox" class="hidden" id="toggle-forms">
				<ul class="sidebar-submenu">
					<li>
						<a href="javascript:void(0);" title="">
							Form control
						</a>
					</li>
					<li>
						<a href="javascript:void(0);" title="">
							Form validation
						</a>
					</li>
					<li>
						<a href="javascript:void(0);" title="">
							Date & time picker
						</a>
					</li>
					<li>
						<a href="javascript:void(0);" title="">
							Color picker
						</a>
					</li>
				</ul>
			</li>
			<li class="has-submenu">
				<label for="toggle-tables">
					<i class="fa fa-table"></i> Tables
				</label>
				<input type="checkbox" class="hidden" id="toggle-tables">
				<ul class="sidebar-submenu">
					<li>
						<a href="javascript:void(0);" title="">
							Tables
						</a>
					</li>
					<li>
						<a href="javascript:void(0);" title="">
							Datatables
						</a>
					</li>
				</ul>
			</li>
			<li class="has-submenu">
				<label for="toggle-charts">
					<i class="fa fa-pie-chart"></i> Charts
				</label>
				<input type="checkbox" class="hidden" id="toggle-charts">
				<ul class="sidebar-submenu">
					<li>
						<a href="javascript:void(0);" title="">
							Chat.js
						</a>
					</li>
					<li>
						<a href="javascript:void(0);" title="">
							Peity charts
						</a>
					</li>
				</ul>
			</li>
			<li class="has-submenu">
				<label for="toggle-maps">
					<i class="fa fa-map"></i> Maps
				</label>
				<input type="checkbox" class="hidden" id="toggle-maps">
				<ul class="sidebar-submenu">
					<li>
						<a href="javascript:void(0);" title="">
							JQV maps
						</a>
					</li>
					<li>
						<a href="javascript:void(0);" title="">
							Google maps
						</a>
					</li>
				</ul>
			</li>
			<li>
				<a href="" title="">
					<i class="fa fa-calendar"></i> Calendar
				</a>
			</li>
			<li class="has-submenu">
				<label for="toggle-elements">
					<i class="fa fa-cube"></i> Elements
				</label>
				<input type="checkbox" class="hidden" id="toggle-elements">
				<ul class="sidebar-submenu">
					<li>
						<a href="javascript:void(0);" title="">
							Alerts
						</a>
					</li>
					<li>
						<a href="javascript:void(0);" title="">
							Buttons
						</a>
					</li>
					<li>
						<a href="javascript:void(0);" title="">
							List groups
						</a>
					</li>
					<li>
						<a href="javascript:void(0);" title="">
							Modals
						</a>
					</li>
					<li>
						<a href="javascript:void(0);" title="">
							Paginations
						</a>
					</li>
					<li>
						<a href="javascript:void(0);" title="">
							Tabs
						</a>
					</li>
				</ul>
			</li>
			<li class="has-submenu">
				<label for="toggle-othes">
					<i class="fa fa-gift"></i> Othes
				</label>
				<input type="checkbox" class="hidden" id="toggle-othes">
				<ul class="sidebar-submenu">
					<li>
						<a href="javascript:void(0);" title="">
							Timeline
						</a>
					</li>
					<li>
						<a href="javascript:void(0);" title="">
							Ribbons
						</a>
					</li>
					<li>
						<a href="javascript:void(0);" title="">
							Hover effects
						</a>
					</li>
				</ul>
			</li>
		</ul>
		<span class="menu-name">
			pages
		</span>
		<ul class="sidebar-menu">
			<li>
				<a href="" title="">
					<i class="fa fa-home"></i> Landing
				</a>
			</li>
			<li class="has-submenu">
				<label for="toggle-account">
					<i class="fa fa-user"></i> Account
				</label>
				<input type="checkbox" class="hidden" id="toggle-account">
				<ul class="sidebar-submenu">
					<li>
						<a href="javascript:void(0);" title="">
							Profile
						</a>
					</li>
					<li>
						<a href="javascript:void(0);" title="">
							Edit profile
						</a>
					</li>
					<li>
						<a href="javascript:void(0);" title="">
							Inbox
						</a>
					</li>
					<li>
						<a href="javascript:void(0);" title="">
							Sign out
						</a>
					</li>
				</ul>
			</li>
			<li>
				<a href="" title="">
					<i class="fa fa-paper-plane"></i> Contact
				</a>
			</li>
			<li>
				<a href="" title="">
					<i class="fa fa-life-ring"></i> FAQ
				</a>
			</li>
			<li>
				<a href="" title="">
					<i class="fa fa-image"></i> Gallery
				</a>
			</li>
			<li>
				<a href="" title="">
					<i class="fa fa-shopping-cart"></i> Orders
				</a>
			</li>
			<li>
				<a href="" title="">
					<i class="fa fa-exclamation-triangle"></i> 404 page
				</a>
			</li>
		</ul>
		<span class="menu-name">
			emails
		</span>
		<ul class="sidebar-menu">
			
			<li class="has-submenu">
				<label for="toggle-email-templates">
					<i class="fa fa-envelope"></i> Email templates
				</label>
				<input type="checkbox" class="hidden" id="toggle-email-templates">
				<ul class="sidebar-submenu">
					<li>
						<a href="javascript:void(0);" title="">
							Action
						</a>
					</li>
					<li>
						<a href="javascript:void(0);" title="">
							Info
						</a>
					</li>
					<li>
						<a href="javascript:void(0);" title="">
							Billing
						</a>
					</li>
				</ul>
			</li>
		</ul>
	</div>
</div>