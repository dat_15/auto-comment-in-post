<?php 

/**
 * 
 */
class assignment_2 extends Controller{
	private $lop;
	private $sinh_vien;
	private $hoc_ky;
	private $giao_vien;
	private $danh_sach_lop;
	private $thoi_khoa_bieu;
	function __construct(){
		parent::__construct();
		$this->sinh_vien = $this->load_model('sinh_vien');
		$this->giao_vien = $this->load_model('giao_vien');
		$this->lop = $this->load_model('lop');
		$this->hoc_ky = $this->load_model('hoc_ky');
		$this->danh_sach_lop = $this->load_model('danh_sach_lop');
		$this->thoi_khoa_bieu = $this->load_model('thoi_khoa_bieu');

	}

	public function index(){
		$this->load_view('head', ['title' => 'Bài 2 - Danh sách lớp']);
        $this->load_view('header');
        $this->load_view('sidebar');
        $page = !empty($_GET['page']) ? (int) $_GET['page'] : 1;
        if ($page < 1) {
        	$page = 1;
        }
        $teacher = !empty($_GET['teacher']) ? $_GET['teacher'] : '';
        $where = [];
        if (!empty($teacher)) {
        	$where['giao_vien'] = $teacher;
        }
        $student = !empty($_GET['student']) ? (int) $_GET['student'] : 0;
        if (!empty($student)) {
        	$where['lop.id IN'] = "SELECT DISTINCT lop FROM `danh_sach_lop` WHERE sinh_vien=$student";
        }
        $class_lists = $this->lop->get_classes($where, PER_PAGE, PER_PAGE*($page-1));
        $pagination = new Pagination([
        	'total' => $class_lists['total'],
        	'perPage' => PER_PAGE,
        	'currentPage' => $page,
        	// 'ulClass' => 'pagination',
        	'link' => DOMAIN . 'assignment-2?'. (!empty($teacher) ? 'teacher='.$teacher.'&' : '' ) . 'page=' 
        ]);
        $this->load_view('bai-2/page-1', [
        	'list'=>$class_lists, 
        	'pagination' => $pagination
        ]);
        $this->load_view('footer');
	}

	public function page_2(){
		$this->load_view('head', ['title' => 'Bài 2 - Danh sách sinh viên']);
        $this->load_view('header');
        $this->load_view('sidebar');
        $page = !empty($_GET['page']) ? (int) $_GET['page'] : 1;
        if ($page < 1) {
        	$page = 1;
        }
        $where = [];
        $class_id = !empty($_GET['class']) ? (int) $_GET['class'] : 0;
        if (!empty($class_id)) {
        	$where['sinh_vien.id IN'] = "SELECT DISTINCT sinh_vien FROM `danh_sach_lop` WHERE lop=$class_id";
        }
        $student_list = $this->sinh_vien->get_students($where, PER_PAGE, PER_PAGE*($page-1));
        $pagination = new Pagination([
        	'total' => $student_list['total'],
        	'perPage' => PER_PAGE,
        	'currentPage' => $page,
        	// 'ulClass' => 'pagination',
        	'link' => DOMAIN . 'assignment-2/page-2?'. (!empty($class_id) ? 'class='.$class_id.'&' : '' ) . 'page=' 
        ]);
        $this->load_view('bai-2/page-2', [
        	'list'=>$student_list, 
        	'pagination' => $pagination
        ]);
        $this->load_view('footer');
	}

	public function page_3(){
		$this->load_view('head', ['title' => 'Bài 2 - Danh sách giảng viên']);
        $this->load_view('header');
        $this->load_view('sidebar');
        $page = !empty($_GET['page']) ? (int) $_GET['page'] : 1;
        if ($page < 1) {
        	$page = 1;
        }
        $teacher_list = $this->giao_vien->get_teachers(PER_PAGE, PER_PAGE*($page-1));
        $pagination = new Pagination([
        	'total' => $teacher_list['total'],
        	'perPage' => PER_PAGE,
        	'currentPage' => $page,
        	// 'ulClass' => 'pagination',
        	'link' => DOMAIN . 'assignment-2/page-3?page=' 
        ]);
        $this->load_view('bai-2/page-3', [
        	'list'=>$teacher_list, 
        	'pagination' => $pagination
        ]);
        $this->load_view('footer');
	}
}

 ?>