<?php

class home extends Controller {
	function __construct(){
		parent::__construct();
	}

	public function index(){
		$this->load_view('head', ['title' => 'Bài 1 - Trang chủ']);
        $this->load_view('header');
        $this->load_view('sidebar');
        $this->load_view('bai-1/page-1');
        $this->load_view('footer');
	}

	public function page_2(){
		$this->load_view('head', ['title' => 'Bài 1 - Page 2']);
        $this->load_view('header');
        $this->load_view('sidebar');
        $this->load_view('bai-1/page-2');
        $this->load_view('footer');
	}

}

/* End of file home.php */
/* Location: .//C/xampp/htdocs/symper/controllers/home.php */