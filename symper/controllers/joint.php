<?php

class joint extends Controller {
	function __construct(){
		parent::__construct();
	}

	public function index(){
		$this->load_view('head', ['title' => 'Bài 3 - JointJS']);
        $this->load_view('header');
        $this->load_view('sidebar');
        $this->load_view('bai-3');
        $this->load_view('footer');
	}

}

/* End of file bai-3.php */
/* Location: .//C/xampp/htdocs/dengo/symper/controllers/bai-3.php */