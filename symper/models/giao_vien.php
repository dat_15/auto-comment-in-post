<?php 
class giao_vien extends Model {
    function __construct(){
        parent::__construct('giao_vien');
    }

    public function get_teachers($per_page, $offset){
    	$res = $this->db->select('*')->group_by('id');
    	$total = $this->db->count_all([]);
    	return ['total' => $total, 'data' => $res->limit($per_page, $offset)->get_rows()];
    }
}
 ?>