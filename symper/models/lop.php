<?php 
class lop extends Model {
    function __construct(){
        parent::__construct('lop');
    }

    public function get_classes($where, $per_page, $offset){
    	$res = $this->db->select('*, gv.id as teacher_id, lop.id as class_id')->where($where)->join([
    		['thoi_khoa_bieu as tkb', 'tkb.lop=lop.id'],
    		['giao_vien as gv', 'tkb.giao_vien=gv.id']
    	])->group_by('lop.id');
    	$total = $this->db->count_all($where);
    	return ['total' => $total, 'data' => $res->limit($per_page, $offset)->get_rows()];
    }
}
 ?>