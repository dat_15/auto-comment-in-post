<?php 
class sinh_vien extends Model {
    function __construct(){
        parent::__construct('sinh_vien');
    }

    public function get_students($where, $per_page, $offset){
    	$res = $this->db->select('*')->where($where)->group_by('id');
    	$total = $this->db->count_all($where);
    	return ['total' => $total, 'data' => $res->limit($per_page, $offset)->get_rows()];
    }
}
 ?>