<?php
/**
* Author : Nguyen Tien Dat.
*/
class Controller
{
    private $data;
    protected $name;
    protected $userInfo;

    public function __construct(){
        $this->name = __CLASS__;
    }

    /**
     * load model và tự tạo biến mới.
     *
     * @param [string] $model [tên model]
     *
     * @return [object] [null hoặc model]
     */
    protected function load_model($name = ''){
        if ($name == '') {
            $name = $this->name;
        }
        if (file_exists('models/'.strtolower($name).'.php')) {
            require 'models/'.strtolower($name).'.php';
            $cls1 = $name;

            return new $cls1();
        } else {
            echo 'Model '.$name.' does not existed';

            return;
        }
    }

    /**
     * Load view trên controller, biến sử dụng bên dạng $ + index của mảng
     * Ex: Controller: $data['title'] = "Page title";
     *     View: echo $title.
     *
     * @param [string] $view  [tên view]
     * @param array    $param [mảng chứa các biến truyền vào view]
     *
     * @return [null] [nếu không tìm thấy file sẽ trả về null]
     */
    protected function load_view($view, $param = array()){
        if ($view == '') {
            $view = $this->name;
        }
        if (file_exists('views/'.$view.'.php')) {
            if (!is_array($param)) {
                $param = is_object($param) ? get_object_vars($param) : array();
            }
            extract($param);
            // include file
            include_once 'views/'.$view.'.php';
        } else {
            echo 'View '.$view.' does not existed';

            return;
        }
    }
}
