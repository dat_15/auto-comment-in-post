<?php
/**
* Author : Nguyen Tien Dat.
*/
class Model{
    protected $db;

    public function __construct($table)    {
        $this->db = new DB($table);
    }

    public function get_all()    {
        return $this->db->get_table();
    }

    public function filter($where = '1', $limit = 1000, $offset = 0)    {
        $total = $this->db->count_all($where);
        $res = $this->db->where($where)->limit($limit, $offset)->get_rows();
        return ['total' => $total, 'data' => $res];
    }

    public function get_row($where = '1'){
        return $this->db->where($where)->get_row();
    }

    public function add($data)    {
        return $this->db->add_row($data);
    }

    public function update($data, $where)    {
        return $this->db->update($data, $where);
    }

    public function delete($where)    {
        return $this->db->delete($where);
    }
}

?>
