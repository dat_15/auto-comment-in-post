<?php 

/**
 * Author: Nguyễn Tiến Đạt
 */
class App{
    public function __construct(){
        $_GET = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);

        if (isset($_GET['url'])) {
            $url = $_GET['url'];
        } else {
            //default controller
            $url = HOME_CONTROLLER;
        }

        $url = explode('/', rtrim($url, '/'));
        $file_name = 'controllers';
        $controller = '';
        $i = 0;
        foreach ($url as $segment) {
            $file_name .= '/'.$segment;
            $remove_hiphens = str_replace('-', '_', $file_name);
            if (file_exists($file_name.'.php') || file_exists($remove_hiphens.'.php')) {
                //Load controller
                if (file_exists($file_name.'.php')) {
                    require $file_name.'.php';
                } else {
                    require $remove_hiphens.'.php';
                }
                // Instance Controller
                $remove_hiphens = str_replace('-', '_', $segment);
                if (class_exists($remove_hiphens)) {
                    $controller = new $remove_hiphens();
                }
                break;
            }
            ++$i;
        }
        if ($i === count($url)) {
            //page 404
            require 'controllers/f404.php';
            $controller = new f404();
            $controller->index();
            return false;
        }
        if (isset($url[$i + 1]) && !empty($url[$i + 1])) {
            $remove_hiphens = str_replace('-', '_', $url[$i + 1]);
            if (
                !method_exists($controller, $url[$i + 1]) &&
                is_numeric($url[$i + 1]) &&
                !method_exists($controller, $remove_hiphens)
            ) {
                $controller->index($url[$i + 1]);
            } elseif (!method_exists($controller, $url[$i + 1]) && !method_exists($controller, $remove_hiphens)) {
                // Khong ton tai method
                require 'controllers/f404.php';
                $controller = new f404();
                $controller->index();
                die;
            } elseif (isset($url[$i + 4])) {
                if (method_exists($controller, $url[$i + 1])) {
                    // method co tham so truyen vao
                    $controller->{$url[$i + 1]}($url[$i + 2], $url[$i + 3], $url[$i + 4]);
                } else {
                    $controller->{$remove_hiphens}($url[$i + 2], $url[$i + 3], $url[$i + 4]);
                }
            } elseif (isset($url[$i + 3])) {
                if (method_exists($controller, $url[$i + 1])) {
                    // method co tham so truyen vao
                    $controller->{$url[$i + 1]}($url[$i + 2], $url[$i + 3]);
                } else {
                    $controller->{$remove_hiphens}($url[$i + 2], $url[$i + 3]);
                }
            } elseif (isset($url[$i + 2])) {
                if (method_exists($controller, $url[$i + 1])) {
                    // method co tham so truyen vao
                    $controller->{$url[$i + 1]}($url[$i + 2]);
                } else {
                    // method co tham so truyen vao
                    $controller->{$remove_hiphens}($url[$i + 2]);
                }
            } else {
                if (method_exists($controller, $url[$i + 1])) {
                    // method co tham so truyen vao
                    $controller->{$url[$i + 1]}();
                } else {
                    // method co tham so truyen vao
                    $controller->{$remove_hiphens}();
                }
            }
        } else {
            //call index function
            $controller->index();
        }
    }
}
?>