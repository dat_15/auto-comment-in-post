<?php
require 'core/config.php';
require 'core/db.php';
require 'core/app.php';
require 'core/controller.php';
require 'core/model.php';
require 'core/pagination.php';

new App();
