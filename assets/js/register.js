var username_guide = "Tên đăng nhập chỉ chứa chữ, số và gạch dưới '_'. Dài từ 8 - 30 ký tự";
var first_name_guide = "Họ chỉ chứa chữ, Dài từ 2 - 25 ký tự";
var last_name_guide = "Tên chỉ chứa chữ, Dài từ 2 - 25 ký tự";
var password_guide = "Mật khẩu phải có 1 chữ hoa, 1 chữ thường, 1 số và 1 trong các ký tự (!@#_). Dài từ 8 - 30 ký tự";
window.onload = function() {
    var emailReg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var passwordReg = /^(?=.*[A-Z])(?=.*[!@#_])(?=.*[0-9])(?=.*[a-z]).{8,30}$/;
    var usernameReg = /^[a-zA-Z0-9_]{8,30}$/;
    var nameReg = /^[a-z ]{4,25}$/;
    var first_name = $("input[name=first_name]");
    var last_name = $("input[name=last_name]");
    var access_token = $('input[name=access-token]').val().trim();
    var username = $("input[name=username]");
    var email = $("input[name=email]");
    var password = $("input[name=password]");
    var re_password = $("input[name=re-password]");
    var submit_btn = $("button.btn.btn-primary.btn-round.btn-lg.btn-block");
    var submit_btn_html = submit_btn.html();
    $(document).ready(function() {
        $("form.form").on('submit', function() {
            event.preventDefault();
            // Check ten truoc
            if (!nameReg.test(nonAccentVietnamese(first_name.val().trim()))) {
                first_name.parent().addClass("form-group-danger").removeClass("form-group-success").click();
                first_name.get(0).setCustomValidity(first_name_guide);
                return;
            }
            if (!nameReg.test(nonAccentVietnamese(last_name.val().trim()))) {
                last_name.parent().addClass("form-group-danger").removeClass("form-group-success").click();
                last_name.get(0).setCustomValidity(last_name_guide);
                return;
            }
            if (!usernameReg.test(username.val().trim())) {
                username.parent().addClass("form-group-danger").removeClass("form-group-success").click();
                username.get(0).setCustomValidity(username_guide);
                return;
            }
            if (!emailReg.test(email.val().trim())) {
                email.parent().addClass("form-group-danger").removeClass("form-group-success").click();
                email.get(0).setCustomValidity("Email không hợp lệ");
                return;
            }
            if (!passwordReg.test(password.val().trim())) {
                password.parent().addClass("form-group-danger").removeClass("form-group-success").click();
                password.get(0).setCustomValidity(password_guide);
                return;
            }
            if (password.val() !== re_password.val()) {
                re_password.parent().addClass("form-group-danger").removeClass("form-group-success").click();
                re_password.get(0).setCustomValidity("Mật khẩu không khớp");
                return;
            }
            submit_btn.attr('disabled', 'disabled').html('<i class="now-ui-icons loader_refresh spin"></i> '+submit_btn_html);
            $.post(API_URL + 'auth/do-registration', {
                token: access_token,
                first_name: first_name.val().trim(),
                last_name: last_name.val().trim(),
                username: username.val().trim(),
                email: email.val().trim(),
                password: password.val().trim(),
                re_password: re_password.val().trim()
            }, function(respon) {
                $('#alert-model').modal('show');
                $('#alert-model .modal-body p').html(respon.msg);
                if (respon.status) {
                    setTimeout(function(){
                        window.location.href = respon.callback_url;
                    }, 2000);
                } else {
                    submit_btn.removeAttr('disabled').html(submit_btn_html);
                }
            });

            return false;
        });
    });
    var timer = null;
    first_name.keyup(function() {
        clearTimeout(timer);
        timer = setTimeout(function() {
            if (!nameReg.test(nonAccentVietnamese(first_name.val().trim()))) {
                first_name.parent().removeClass("form-group-success");
                first_name.parent().addClass("form-group-danger").click();
                first_name.get(0).setCustomValidity(first_name_guide);
            } else {
                first_name.parent().removeClass("form-group-danger")
                first_name.parent().addClass("form-group-success");
                first_name.get(0).setCustomValidity('');
            }
        }, 500);
    });
    last_name.keyup(function() {
        clearTimeout(timer);
        timer = setTimeout(function() {
            if (!nameReg.test(nonAccentVietnamese(last_name.val().trim()))) {
                last_name.parent().addClass("form-group-danger");
                last_name.parent().removeClass("form-group-success");
                last_name.get(0).setCustomValidity(last_name_guide);
            } else {
                last_name.parent().removeClass("form-group-danger")
                last_name.parent().addClass("form-group-success");
                last_name.get(0).setCustomValidity('');
            }
        }, 500);
    });
    password.keyup(function() {
        clearTimeout(timer);
        timer = setTimeout(function() {
            if (!passwordReg.test(password.val().trim())) {
                password.parent().addClass("form-group-danger");
                password.parent().removeClass("form-group-success");
                password.get(0).setCustomValidity(password_guide);
            } else {
                password.parent().removeClass("form-group-danger")
                password.parent().addClass("form-group-success");
                password.get(0).setCustomValidity('');
            }
        }, 500);
    });
    re_password.keyup(function() {
        clearTimeout(timer);
        timer = setTimeout(function() {
            if (!passwordReg.test(re_password.val().trim())) {
                re_password.parent().addClass("form-group-danger");
                re_password.parent().removeClass("form-group-success");
                re_password.get(0).setCustomValidity(password_guide);
            } else {
                if (password.val() !== re_password.val()) {
                    re_password.parent().addClass("form-group-danger").removeClass("form-group-success");
                    re_password.get(0).setCustomValidity("Mật khẩu không khớp");
                } else {
                    re_password.parent().removeClass("form-group-danger").addClass("form-group-success");
                    re_password.get(0).setCustomValidity('');
                }
            }
        }, 500);
    });
    // Check username exist
    username.keyup(function() {

        if (!usernameReg.test(username.val().trim())) {
            username.parent().addClass("form-group-danger");
            username.parent().removeClass("form-group-success");
            username.get(0).setCustomValidity(username_guide);
        } else {
            clearTimeout(timer);
            timer = setTimeout(function() {
                $.post(API_URL + 'auth/check-username-exist', {
                    token: access_token,
                    username: username.val().trim()
                }, function(_res) {
                    if (!_res.status) {
                        $('#alert-model').modal('show');
                        $('#alert-model .modal-body p').html(username_guide);
                        username.parent().addClass("form-group-danger");
                        username.get(0).setCustomValidity(username_guide);
                    } else {
                        if (typeof(_res.is_exist) == 'number' && _res.is_exist) {
                            username.parent().addClass("form-group-danger");
                            username.parent().removeClass("form-group-success")
                            $('#alert-model').modal('show');
                            $('#alert-model .modal-body p').html('Tên đăng nhập đã tồn tại');
                            username.get(0).setCustomValidity('Tên đăng nhập đã tồn tại');
                        } else {
                            username.parent().removeClass("form-group-danger")
                            username.parent().addClass("form-group-success");
                            username.get(0).setCustomValidity('');
                        }
                    }
                });
            }, 500);
        }
    });
    // Check enail exist
    email.keyup(function() {

        if (!emailReg.test(email.val().trim())) {
            email.parent().addClass("form-group-danger");
            email.parent().removeClass("form-group-success");
        } else {
            clearTimeout(timer);
            timer = setTimeout(function() {
                $.post(API_URL + 'auth/check-email-exist', {
                    token: access_token,
                    email: email.val().trim()
                }, function(_res) {
                    if (!_res.status) {
                        $('#alert-model').modal('show');
                        $('#alert-model .modal-body p').html("Email không hợp lệ");
                        email.parent().addClass("form-group-danger");
                    } else {
                        if (typeof(_res.is_exist) == 'number' && _res.is_exist) {
                            email.parent().addClass("form-group-danger");
                            email.parent().removeClass("form-group-success")
                            $('#alert-model').modal('show');
                            $('#alert-model .modal-body p').html('Email đã tồn tại ');
                            email.get(0).setCustomValidity('Email đã tồn tại');
                        } else {
                            email.parent().removeClass("form-group-danger")
                            email.parent().addClass("form-group-success");
                            email.get(0).setCustomValidity('');
                        }
                    }
                });
            }, 500);
        }
    });

    // Activate the image for the navbar-collapse
    var navbar_menu_visible = 0;
    $('input.form-control').on("focus", function() {
        $(this).parent('.input-group').addClass("input-group-focus");
    }).on("blur", function() {
        if (typeof($(this).get(0).nodeName) != 'undefined' && $(this).get(0).nodeName == 'INPUT') {
            $(this).get(0).setCustomValidity("");
            $(this).parent(".input-group").removeClass("input-group-focus form-group-danger");
        }
    });
    $(document).on('click', '.navbar-toggler', function() {
        var $toggle = $(this);

        if (navbar_menu_visible == 1) {
            $('html').removeClass('nav-open');
            navbar_menu_visible = 0;
            $('#bodyClick').remove();
            setTimeout(function() {
                $toggle.removeClass('toggled');
            }, 550);
        } else {
            setTimeout(function() {
                $toggle.addClass('toggled');
            }, 580);
            div = '<div id="bodyClick"></div>';
            $(div).appendTo('body').click(function() {
                $('html').removeClass('nav-open');
                navbar_menu_visible = 0;
                setTimeout(function() {
                    $toggle.removeClass('toggled');
                    $('#bodyClick').remove();
                }, 550);
            });
            $('html').addClass('nav-open');
            navbar_menu_visible = 1;
        }
    });
};

function nonAccentVietnamese(str) {
    str = str.toLowerCase();
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/\u00E0|\u00E1|\u1EA1|\u1EA3|\u00E3|\u00E2|\u1EA7|\u1EA5|\u1EAD|\u1EA9|\u1EAB|\u0103|\u1EB1|\u1EAF|\u1EB7|\u1EB3|\u1EB5/g, "a");
    str = str.replace(/\u00E8|\u00E9|\u1EB9|\u1EBB|\u1EBD|\u00EA|\u1EC1|\u1EBF|\u1EC7|\u1EC3|\u1EC5/g, "e");
    str = str.replace(/\u00EC|\u00ED|\u1ECB|\u1EC9|\u0129/g, "i");
    str = str.replace(/\u00F2|\u00F3|\u1ECD|\u1ECF|\u00F5|\u00F4|\u1ED3|\u1ED1|\u1ED9|\u1ED5|\u1ED7|\u01A1|\u1EDD|\u1EDB|\u1EE3|\u1EDF|\u1EE1/g, "o");
    str = str.replace(/\u00F9|\u00FA|\u1EE5|\u1EE7|\u0169|\u01B0|\u1EEB|\u1EE9|\u1EF1|\u1EED|\u1EEF/g, "u");
    str = str.replace(/\u1EF3|\u00FD|\u1EF5|\u1EF7|\u1EF9/g, "y");
    str = str.replace(/\u0111/g, "d");
    str = str.replace(/\u0300|\u0301|\u0303|\u0309|\u0323/g, ""); // Huyền sắc hỏi ngã nặng 
    str = str.replace(/\u02C6|\u0306|\u031B/g, ""); // Â, Ê, Ă, Ơ, Ư
    return str;
}