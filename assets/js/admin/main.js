function main_script() {
    window.fbAsyncInit = function() {
        FB.init({
            appId: '196731451008310',
            cookie: true,
            xfbml: true,
            version: 'v3.0'
        });

        FB.AppEvents.logPageView();

    };

    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) { return; }
        js = d.createElement(s);
        js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    var _token = $('input[name=access-token]').val().trim();
    function show_alert(msg){
        $.notify({
            icon: "now-ui-icons ui-1_bell-53",
            message: msg

        },{
            type: 'primary',
            timer: 4000,
            placement: {
                from: 'top',
                align: 'right'
            }
        });
    }
    var vm = new Vue({
        el: '.main-panel',
        data: {
            title: 'Xin chào!',
            description: 'Hãy liên kết với tài khoản facebook để sử dụng tính năng tự động bình luận theo nội dung được cài đặt trước!',
            show_loading: true,
            is_connected: false,
            pages: []
        },
        methods: {
            login_fb: function() {
                var _this = this;
                _this.show_loading = true;
                FB.login(function(response) {
                    if (response.status == 'connected') {
                        _this.is_connected = true;
                        $.post(
                            API_URL + 'facebook/set-user-account/', {
                                fb_id: response.authResponse.userID,
                                access_token: response.authResponse.accessToken,
                                token: $('input[name=access-token]').val().trim()
                            },
                            function(res) {
                                _this.title = 'Chọn page để cài đặt!';
                                _this.description = 'Tài khoản của bạn đã được liên kết tới facebook thành công. Hãy chọn page để cài đặt nội dung trả lời tự động!';
                                _this.get_fb_page();
                            }
                        );
                    }else{
                        setTimeout(function(){
                            _this.show_loading = false;
                        }, 500);
                    }
                }, { scope: 'public_profile,email,manage_pages,publish_pages,read_page_mailboxes' });
                // }, { scope: 'email' });
            },
            get_fb_page: function() {
                var __this = this;
                $.get(
                    API_URL + 'facebook/get-fb-page/' + _token,
                    function(_res) {
                        if (typeof _res !== 'object') {
                            _res = JSON.parse(_res);
                        }
                        if (_res.data != undefined) {
                            __this.pages = _res.data.data;
                            __this.show_loading = false;
                        }
                    }
                );
            },
            
            subscribe_page: function(fanpage_id, e) {
                var ___this = $(e.target);
                if (e.target.parentNode.nodeName == 'BUTTON') ___this = ___this.parent();
                ___this.html('<i class="now-ui-icons loader_refresh spin"></i>');
                for (var i = 0; i < this.pages.length; i++) {
                    if (this.pages[i].fanpage_id == fanpage_id) {
                        $.get(
                            API_URL + 'facebook/subscribe-app/' + this.pages[i].fanpage_id + '/' + _token,
                            function(_res) {
                                if (typeof _res !== 'object') {
                                    _res = JSON.parse(_res);
                                }
                                if (_res.success != undefined && _res.success == true) {
                                    ___this.html('<i class="now-ui-icons media-1_button-pause"></i>');
                                    show_alert('Đăng ký fanpage thành công');
                                } else {
                                    ___this.html('<i class="now-ui-icons media-1_button-play"></i>');
                                    show_alert(_res.error.message);
                                }
                            }
                        );
                        this.pages[i].is_subscribed = 1;
                    }
                }
            },
            unsubscribe_page: function(fanpage_id, e){
                var ___this = $(e.target);
                if (e.target.parentNode.nodeName == 'BUTTON') ___this = ___this.parent();
                ___this.html('<i class="now-ui-icons loader_refresh spin"></i>');
                for (var i = 0; i < this.pages.length; i++) {
                    if (this.pages[i].fanpage_id == fanpage_id) {
                        $.get(
                            API_URL + 'facebook/unsubscribe-app/' + this.pages[i].fanpage_id + '/' + _token,
                            function(_res) {
                                if (typeof _res !== 'object') {
                                    _res = JSON.parse(_res);
                                }
                                if (_res.success != undefined && _res.success == true) {
                                    ___this.html('<i class="now-ui-icons media-1_button-play"></i>');
                                    show_alert('Hủy đăng ký fanpage thành công');
                                } else {
                                    ___this.html('<i class="now-ui-icons media-1_button-pause"></i>');
                                    show_alert(_res.error.message);
                                }
                            }
                        );
                        this.pages[i].is_subscribed = 0;
                    }
                }
            }
        }
    });
    $.get(
        API_URL + 'facebook/get-facebook-account/' + _token,
        function(_res) {
            if (typeof _res !== 'object') {
                _res = JSON.parse(_res);
            }
            if (_res.is_connected != undefined) {
                vm.is_connected = _res.is_connected;
                if (!_res.is_connected) {
                    setTimeout(() => {
                        vm.show_loading = false;
                    }, 1000);
                } else {
                    vm.title = 'Chọn page để cài đặt!';
                    vm.description = 'Tài khoản của bạn đã được liên kết tới facebook thành công. Hãy chọn page để cài đặt nội dung trả lời tự động!';
                    vm.get_fb_page();
                }
            }
            if(!_res.status){
            	console.log(_res);
            }
        }
    );
}

window.onload = function(){
    main_script();
}