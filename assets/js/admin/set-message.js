function set_up_content() {
    var _token = $('input[name=access-token]').val().trim();
    function show_alert(msg){
        $.notify({
            icon: "now-ui-icons ui-1_bell-53",
            message: msg

        },{
            type: 'primary',
            timer: 4000,
            placement: {
                from: 'top',
                align: 'right'
            }
        });
    }
    function nonAccentVietnamese(str) {
        str = str.toLowerCase();
        str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
        str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
        str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
        str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
        str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
        str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
        str = str.replace(/đ/g, "d");
        str = str.replace(/\u00E0|\u00E1|\u1EA1|\u1EA3|\u00E3|\u00E2|\u1EA7|\u1EA5|\u1EAD|\u1EA9|\u1EAB|\u0103|\u1EB1|\u1EAF|\u1EB7|\u1EB3|\u1EB5/g, "a");
        str = str.replace(/\u00E8|\u00E9|\u1EB9|\u1EBB|\u1EBD|\u00EA|\u1EC1|\u1EBF|\u1EC7|\u1EC3|\u1EC5/g, "e");
        str = str.replace(/\u00EC|\u00ED|\u1ECB|\u1EC9|\u0129/g, "i");
        str = str.replace(/\u00F2|\u00F3|\u1ECD|\u1ECF|\u00F5|\u00F4|\u1ED3|\u1ED1|\u1ED9|\u1ED5|\u1ED7|\u01A1|\u1EDD|\u1EDB|\u1EE3|\u1EDF|\u1EE1/g, "o");
        str = str.replace(/\u00F9|\u00FA|\u1EE5|\u1EE7|\u0169|\u01B0|\u1EEB|\u1EE9|\u1EF1|\u1EED|\u1EEF/g, "u");
        str = str.replace(/\u1EF3|\u00FD|\u1EF5|\u1EF7|\u1EF9/g, "y");
        str = str.replace(/\u0111/g, "d");
        str = str.replace(/\u0300|\u0301|\u0303|\u0309|\u0323/g, ""); // Huyền sắc hỏi ngã nặng 
        str = str.replace(/\u02C6|\u0306|\u031B/g, ""); // Â, Ê, Ă, Ơ, Ư
        return str;
    }
    var vm = new Vue({
        el: '.main-panel',
        data: {
            title: 'Thiết lập nội dung tin nhắn!',
            description: 'Thật tuyệt! Tài khoản của bạn đã liên kết với facebook thành công!<br>Hãy bắt đầu thiết lập bài post hoặc ảnh để tự động với nội dung mặc định!',
            show_loading: true,
            is_connected: false,
            is_editting: false,
            cur_post_id: '',
            subscribed_pages: [],
            list_posts: [],
            list_posts_id: [],
            model: {
                title: 'Chọn fanpage',
                step: 1,
                posts: [],
                result_posts: [],
                photos: [],
                result_photos: [],
                selected_page_id: '',
                selected_posts: [],
                selected_photos: [],
                message_reply: '',
                comment_reply: '',
                show_loading: false
            }
        },
        methods: {
            get_subscribed_pages: function() {
                var __this = this;
                $.get(
                    API_URL + 'facebook/get-subscribed-page/' + _token,
                    function(_res) {
                        if (typeof _res !== 'object') {
                            _res = JSON.parse(_res);
                        }
                        if (_res.is_connected != undefined) {
                            __this.is_connected = _res.is_connected;
                            if (!_res.is_connected) {
                                __this.title = 'Opps! Tài khoản chưa liên kết với facebook';
                                __this.description = 'Để thực hiện thao tác cài đặt này bạn cần liên kết với tài khoản facebook và đăng ký fanpage để tự động trả lời bình luận.';
                                setTimeout(() => {
                                    __this.show_loading = false;
                                }, 1000);
                            } else{
                                if ( !parseInt(_res.data.total) ) {
                                    __this.title = 'Oops! Bạn chưa đăng ký fanpage nào!';
                                    __this.description = 'Để thực hiện thao tác cài đặt này bạn cần liên kết với tài khoản facebook và đăng ký fanpage để tự động trả lời bình luận.';
                                }
                                if (_res.data != undefined) {
                                    __this.subscribed_pages = _res.data.data;
                                    __this.get_all_posts();
                                }
                            }
                        }
                        if(!_res.status){
                            show_alert(_res.msg);
                        }
                    }
                );
            },
            get_all_posts: function(){
                var __this_app = this;
                $.get(
                    API_URL + 'facebook/get-subscribed-posts/' + _token, 
                    function(__res){
                        __this_app.show_loading = false;
                        if (typeof __res !== 'object') {
                            __res = JSON.parse(__res);
                        }
                        if(__res.status){
                            if(typeof __res.data === 'object'){
                                __this_app.list_posts = __res.data;
                                for (var i = 0; i < __res.data.length; i++) {
                                    __this_app.list_posts_id.push(__res.data[i].post_id);
                                }
                            }
                        }else{
                            show_alert(__res.msg);
                        }
                    }
                );
            },
            get_fanpage_posts: function(){
                var __this_app = this;
                $.get(
                    API_URL + 'facebook/get-page-posts/' + __this_app.model.selected_page_id + '/' + _token, 
                    function(__res){
                        __this_app.show_loading = false;
                        if (typeof __res !== 'object') {
                            __res = JSON.parse(__res);
                        }
                        if(__res.status){
                            if(typeof __res.data === 'object'){
                                __this_app.model.show_loading = false;
                                __this_app.model.posts = [];
                                __this_app.model.result_posts = [];
                                for (var i = 0; i < __res.data.data.length; i++) {
                                    if (__this_app.list_posts_id.indexOf(__res.data.data[i].id) < 0) {
                                        __this_app.model.posts.push(__res.data.data[i]);
                                        __this_app.model.result_posts.push(__res.data.data[i]);
                                    }
                                }
                            }
                        }else{
                            show_alert(__res.msg);
                        }
                    }
                );
            },
            get_fanpage_photos: function(){
                var __this_app = this;
                $.get(
                    API_URL + 'facebook/get-page-photos/' + __this_app.model.selected_page_id + '/' + _token, 
                    function(__res){
                        __this_app.show_loading = false;
                        if (typeof __res !== 'object') {
                            __res = JSON.parse(__res);
                        }
                        if(__res.status){
                            if(typeof __res.data === 'object'){
                                __this_app.model.show_loading = false;
                                __this_app.model.result_photos = [];
                                __this_app.model.photos = [];
                                for (var i = 0; i < __res.data.data.length; i++) {
                                    if (__this_app.list_posts_id.indexOf(__res.data.data[i].id) < 0) {
                                        __this_app.model.photos.push(__res.data.data[i]);
                                        __this_app.model.result_photos.push(__res.data.data[i]);
                                    }
                                }
                            }
                        }else{
                            show_alert(__res.msg);
                        }
                    }
                );
            },

            select_page: function (fanpage_id, e){
                this.model.selected_page_id = fanpage_id;
                this.model.step = 2;
                this.model.title = 'Vui lòng chọn sản phẩm.<br><small>Bạn có thể chọn nhiều post hoặc ảnh</small>';
                this.model.show_loading = true;
                this.model.selected_posts = [];
                this.model.selected_photos = [];
                this.get_fanpage_posts();
                this.get_fanpage_photos();
            },

            select_posts: function(post, e){
                for (var i = 0; i < this.model.selected_posts.length; i++) {
                    if (this.model.selected_posts[i].id == post.id) {
                        this.model.selected_posts.splice(i, 1);
                        return;
                    }
                }
                this.model.selected_posts.push(post);
            },

            is_selected_post: function(post){
                for (var i = 0; i < this.model.selected_posts.length; i++) {
                    if (this.model.selected_posts[i].id == post.id) {
                        return true;
                    }
                }
                return false;
            },

            select_photos: function(photo, e){
                e.preventDefault();
                e.stopPropagation();
                $(e.target).toggleClass('active');
                $(e.target).parent().toggleClass('active');
                for (var i = 0; i < this.model.selected_photos.length; i++) {
                    if (this.model.selected_photos[i].id == photo.id) {
                        this.model.selected_photos.splice(i, 1);
                        return;
                    }
                }
                this.model.selected_photos.push(photo);
            },

            is_selected_photo: function(photo){
                for (var i = 0; i < this.model.selected_photos.length; i++) {
                    if (this.model.selected_photos[i].id == photo.id) {
                        return true;
                    }
                }
                return false;
            },

            goToNextStep: function(){
                this.model.title = 'Thiết lập nội dung.<br><small>Trả lời qua tin nhắn và bình luận</small>';
                if (!this.model.selected_photos.length && !this.model.selected_posts.length) {
                    show_alert('Vui lòng chọn ít nhất một bài post hoặc 1 bức ảnh để tiếp tục!');
                    return;
                }
                this.model.step = 3;
            },

            backToPreviosStep: function (){
                var step = this.model.step;
                if (step < 2) {
                    return;
                }
                this.model.step--;
                if(this.model.step == 2){
                    this.model.title = 'Vui lòng chọn sản phẩm.<br><small>Bạn có thể chọn nhiều post hoặc ảnh</small>';
                }else{
                    this.model.title = 'Chọn fanpage';
                }
            },

            addPosts: function (){
                this.model.show_loading = true;
                this.model.title = 'Xin vui lòng đợi giây lát.<br><small>Bài post và ảnh đang được lưu lại!</small>';
                var _this = this;
                if (!this.model.selected_photos.length && !this.model.selected_posts.length) {
                    show_alert('Vui lòng chọn ít nhất một bài post hoặc 1 bức ảnh để tiếp tục!');
                    this.model.step = 2;
                    this.model.title = 'Vui lòng chọn sản phẩm.<br><small>Bạn có thể chọn nhiều post hoặc ảnh</small>';
                    return;
                }
                if (this.model.comment_reply.trim() == '' || this.model.message_reply.trim().length == ''){
                    show_alert('Vui lòng nhập nội dung trả lời bình luận và tin nhắn đầy đủ!');
                    return;
                }
                $.post(API_URL+'facebook/add-subscribed-posts', {
                    posts: _this.model.selected_posts,
                    photos: _this.model.selected_photos,
                    fanpage_id: _this.model.selected_page_id,
                    comment_reply: _this.model.comment_reply,
                    message_reply: _this.model.message_reply,
                    token: _token
                }, function(res_content) {
                    if (typeof res_content !== 'object') {
                        res_content = JSON.parse(res_content);
                    }
                    if (res_content.status) {
                        for (var i = 0; i < res_content.posts.length; i++) {
                            if(
                                res_content.posts[i] === '' || 
                                res_content.posts[i] === null || 
                                Object.getOwnPropertyNames(res_content.posts[i]).length === 0 
                            ) continue;
                            _this.list_posts.push(res_content.posts[i]);
                            _this.list_posts_id.push(res_content.posts[i].post_id);
                        }
                        _this.model = {
                            title: 'Chọn fanpage',
                            step: 1,
                            posts: [],
                            result_posts: [],
                            photos: [],
                            result_posts: [],
                            selected_page_id: '',
                            selected_posts: [],
                            selected_photos: [],
                            message_reply: '',
                            comment_reply: '',
                            show_loading: true
                        };
                        $("#noticeModal").modal('hide');
                    }
                    show_alert(res_content.msg);
                    setTimeout(function(){
                        _this.model.show_loading = false;
                    }, 500);
                });
            },

            show_editing: function (post){
                this.is_editting = true;
                this.cur_post_id = post.post_id;
            },

            save_post: function (post){
                this.show_loading = true;
                var _this = this;
                $.post(API_URL+'facebook/update-subscribed-posts', {
                    post_id: post.post_id,
                    is_running: post.is_running,
                    parent_id: post.parent_id,
                    comment_reply: post.comment_reply,
                    message_reply: post.message_reply,
                    token: _token
                }, function(res_content) {
                    if (typeof res_content !== 'object') {
                        res_content = JSON.parse(res_content);
                    }
                    if (res_content.status) {
                        _this.is_editting = false;
                        _this.cur_post_id = '';
                        setTimeout(function(){
                            _this.show_loading = false;
                        }, 500);
                    }
                    show_alert(res_content.msg);
                });
            },
            del_post: function (post_id){
                var _this = this;
                $.post(API_URL+'facebook/delete-subscribed-posts', {
                    post_id: post_id,
                    token: _token
                }, function(res_content) {
                    if (typeof res_content !== 'object') {
                        res_content = JSON.parse(res_content);
                    }
                    if (res_content.status) {
                        _this.list_posts = _this.list_posts.filter(function(p){
                            return p.post_id !== post_id;
                        });
                        _this.list_posts_id = _this.list_posts_id.filter(function(p){
                            return p !== post_id;
                        });
                    }
                    show_alert(res_content.msg);
                });
            },
            find_post: function(e){
                if (e.target.value.trim().length === 0) {
                    this.model.result_posts = this.model.posts;
                }
                var key = e.target.value.trim().toLowerCase();
                this.model.result_posts = this.model.posts.filter(function(p) {
                    return ( typeof p.message !== 'undefined' && nonAccentVietnamese(p.message.toLowerCase()).includes(key) ) || 
                    ( typeof p.story !== 'undefined' && nonAccentVietnamese(p.story.toLowerCase()).includes(key) );
                });
            }
        }
    });
    vm.get_subscribed_pages();
};

window.onload = function(){
    set_up_content();
};