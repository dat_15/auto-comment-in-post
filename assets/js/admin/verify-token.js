function verify_token_script() {
    window.fbAsyncInit = function() {
        FB.init({
            appId: '196731451008310',
            cookie: true,
            xfbml: true,
            version: 'v3.0'
        });

        FB.AppEvents.logPageView();

    };

    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) { return; }
        js = d.createElement(s);
        js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    var _token = $('input[name=access-token]').val().trim();
    function show_alert(msg){
        $.notify({
            icon: "now-ui-icons ui-1_bell-53",
            message: msg

        },{
            type: 'primary',
            timer: 4000,
            placement: {
                from: 'top',
                align: 'right'
            }
        });
    }
    function show_success(msg){
        $.notify({
            icon: "now-ui-icons ui-1_bell-53",
            message: msg

        },{
            type: 'success',
            timer: 4000,
            placement: {
                from: 'top',
                align: 'right'
            }
        });
    }
    var vm = new Vue({
        el: '.main-panel',
        data: {
            show_loading: false
        },
        methods: {
            login_fb: function() {
                var _this = this;
                _this.show_loading = true;
                FB.login(function(response) {
                    if (response.status == 'connected') {
                        $.post(
                            API_URL + 'facebook/verify-token', {
                                fb_id: response.authResponse.userID,
                                access_token: response.authResponse.accessToken,
                                token: $('input[name=access-token]').val().trim()
                            },
                            function(res) {
                                if (typeof res !== 'object') {
                                    res = JSON.parse(res);
                                }
                                show_success(res.msg);
                                setTimeout(function(){
                                    _this.show_loading = false;
                                }, 500);
                            }
                        );
                    }else{
                        setTimeout(function(){
                            _this.show_loading = false;
                        }, 500);
                    }
                // }, { scope: 'public_profile,email,manage_pages,publish_pages,read_page_mailboxes' });
                }, { scope: 'public_profile,email,manage_pages'});
            }
        }
    });
}

window.onload = function(){
    verify_token_script();
}