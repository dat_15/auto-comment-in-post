window.onload = function() {
    var access_token = $('input[name=access-token]').val().trim();
    var email = $("input[name=email]");
    var emailReg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    $(document).ready(function() {
        $("form.form").on('submit', function() {
            event.preventDefault();
            if (!emailReg.test(email.val().trim())) {
                email.parent().addClass("form-group-danger").removeClass("form-group-success").click();
                email.get(0).setCustomValidity("Email không hợp lệ");
                $('.alert').html('Email không hợp lệ');
                return;
            }
            $.post(API_URL + 'auth/send-email-forgot-password', {
                token: access_token,
                username: username.val().trim(),
                password: password.val().trim(),
            }, function(respon) {
                $('.alert').html(respon.msg);
            });

            return false;
        });
    });
    var timer = null;
    // Check enail exist
    email.keyup(function() {

        if (!emailReg.test(email.val().trim())) {
            email.parent().addClass("form-group-danger");
            email.parent().removeClass("form-group-success");
        } else {
            clearTimeout(timer);
            timer = setTimeout(function() {
                $.post(API_URL + 'auth/check-email-exist', {
                    token: access_token,
                    email: email.val().trim()
                }, function(_res) {
                    if (!_res.status) {
                        $('.alert').html("Email không hợp lệ");
                        email.parent().addClass("form-group-danger");
                    } else {
                        if (typeof(_res.is_exist) == 'number' && !_res.is_exist) {
                            email.parent().addClass("form-group-danger");
                            email.parent().removeClass("form-group-success")
                            $('.alert').html('Email không tồn tại ');
                            email.get(0).setCustomValidity('Email không tồn tại');
                        } else {
                            email.parent().removeClass("form-group-danger")
                            email.parent().addClass("form-group-success");
                            email.get(0).setCustomValidity('');
                        }
                    }
                });
            }, 500);
        }
    });
    // Activate the image for the navbar-collapse
    var navbar_menu_visible = 0;
    $('input.form-control').on("focus", function() {
        $(this).parent('.input-group').addClass("input-group-focus");
    }).on("blur", function() {
        if (typeof($(this).get(0).nodeName) != 'undefined' && $(this).get(0).nodeName == 'INPUT') {
            $(this).get(0).setCustomValidity("");
            $(this).parent(".input-group").removeClass("input-group-focus form-group-danger");
        }
    });
    $(document).on('click', '.navbar-toggler', function() {
        var $toggle = $(this);

        if (navbar_menu_visible == 1) {
            $('html').removeClass('nav-open');
            navbar_menu_visible = 0;
            $('#bodyClick').remove();
            setTimeout(function() {
                $toggle.removeClass('toggled');
            }, 550);
        } else {
            setTimeout(function() {
                $toggle.addClass('toggled');
            }, 580);
            div = '<div id="bodyClick"></div>';
            $(div).appendTo('body').click(function() {
                $('html').removeClass('nav-open');
                navbar_menu_visible = 0;
                setTimeout(function() {
                    $toggle.removeClass('toggled');
                    $('#bodyClick').remove();
                }, 550);
            });
            $('html').addClass('nav-open');
            navbar_menu_visible = 1;
        }
    });
};