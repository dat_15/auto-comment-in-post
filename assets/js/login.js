var username_guide = "Tên đăng nhập chỉ chứa chữ, số và gạch dưới '_'. Dài từ 8 - 30 ký tự";
var password_guide = "Mật khẩu phải có 1 chữ hoa, 1 chữ thường, 1 số và 1 trong các ký tự (!@#_). Dài từ 8 - 30 ký tự";
window.onload = function() {
    
    var passwordReg = /^(?=.*[A-Z])(?=.*[!@#_])(?=.*[0-9])(?=.*[a-z]).{8,30}$/;
    var usernameReg = /^[a-zA-Z0-9_]{8,30}$/;
    var access_token = $('input[name=access-token]').val().trim();
    var username = $("input[name=username]");
    var password = $("input[name=password]");
    var submit_btn = $("button.btn.btn-primary.btn-round.btn-lg.btn-block");
    var submit_btn_html = submit_btn.html();
    $(document).ready(function() {
        $("form.form").on('submit', function() {
            event.preventDefault();

            if (!usernameReg.test(username.val().trim())) {
                username.parent().addClass("form-group-danger").removeClass("form-group-success").click();
                username.get(0).setCustomValidity(username_guide);
                return;
            }

            if (!passwordReg.test(password.val().trim())) {
                password.parent().addClass("form-group-danger").removeClass("form-group-success").click();
                password.get(0).setCustomValidity(password_guide);
                return;
            }
            submit_btn.attr('disabled', 'disabled').html('<i class="now-ui-icons loader_refresh spin"></i> '+submit_btn_html);
            $.post(API_URL + 'auth/do-login', {
                token: access_token,
                username: username.val().trim(),
                password: password.val().trim(),
            }, function(respon) {
                $('#alert-model').modal('show');
                $('#alert-model .modal-body p').html(respon.msg);
                if (respon.status) {
                    setTimeout(function(){
                        window.location.href = respon.callback_url;
                    }, 2000);
                } else {
                    submit_btn.removeAttr('disabled').html(submit_btn_html);
                }
            });

            return false;
        });
    });
    var timer = null;
    // Activate the image for the navbar-collapse
    var navbar_menu_visible = 0;
    $('input.form-control').on("focus", function() {
        $(this).parent('.input-group').addClass("input-group-focus");
    }).on("blur", function() {
        if (typeof($(this).get(0).nodeName) != 'undefined' && $(this).get(0).nodeName == 'INPUT') {
            $(this).get(0).setCustomValidity("");
            $(this).parent(".input-group").removeClass("input-group-focus form-group-danger");
        }
    });
    $(document).on('click', '.navbar-toggler', function() {
        var $toggle = $(this);

        if (navbar_menu_visible == 1) {
            $('html').removeClass('nav-open');
            navbar_menu_visible = 0;
            $('#bodyClick').remove();
            setTimeout(function() {
                $toggle.removeClass('toggled');
            }, 550);
        } else {
            setTimeout(function() {
                $toggle.addClass('toggled');
            }, 580);
            div = '<div id="bodyClick"></div>';
            $(div).appendTo('body').click(function() {
                $('html').removeClass('nav-open');
                navbar_menu_visible = 0;
                setTimeout(function() {
                    $toggle.removeClass('toggled');
                    $('#bodyClick').remove();
                }, 550);
            });
            $('html').addClass('nav-open');
            navbar_menu_visible = 1;
        }
    });
};