-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 01, 2018 at 01:21 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dengo`
--

-- --------------------------------------------------------

--
-- Table structure for table `_dengo_products`
--

CREATE TABLE `_dengo_products` (
  `p_id` int(11) NOT NULL,
  `p_title` varchar(255) NOT NULL,
  `p_short_des` varchar(1000) NOT NULL,
  `p_description` text NOT NULL,
  `p_thumb_id` int(11) NOT NULL,
  `p_slug` varchar(255) NOT NULL,
  `p_sale_price` int(11) NOT NULL DEFAULT '1500000',
  `p_origin_price` int(11) DEFAULT '1500000',
  `p_categories` varchar(255) DEFAULT NULL,
  `p_tags` varchar(255) DEFAULT NULL,
  `p_gallery` varchar(255) DEFAULT NULL,
  `p_date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `_dengo_products`
--

INSERT INTO `_dengo_products` (`p_id`, `p_title`, `p_short_des`, `p_description`, `p_thumb_id`, `p_slug`, `p_sale_price`, `p_origin_price`, `p_categories`, `p_tags`, `p_gallery`, `p_date_created`) VALUES
(1, 'Sản phẩm test', 'Đây là sản phẩm test thôi', 'Đây là sản phẩm test thôi', 1, 'san-pham-test', 1500000, 1500000, NULL, NULL, NULL, '2018-07-01 08:22:13'),
(2, 'Sản phẩm test', 'Đây là sản phẩm test thôi', 'Đây là sản phẩm test thôi', 1, 'san-pham-test', 1500000, 1500000, NULL, NULL, NULL, '2018-07-01 08:37:20');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `_dengo_products`
--
ALTER TABLE `_dengo_products`
  ADD PRIMARY KEY (`p_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `_dengo_products`
--
ALTER TABLE `_dengo_products`
  MODIFY `p_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
