<style>
    @media only screen and (max-width:460px){
        .login-page .card-login .logo-container{
            margin-bottom:0;
            width:40px;
        }
    }
    .form-group-danger {
        background-color: rgba(255, 108, 0, 0.3);
        border-radius: 30px;
    }
    .input-group.form-group-success:after {
        font-family: 'Nucleo Outline';
        content: "\ea22";
        display: inline-block;
        position: absolute;
        right: 20px;
        top: 15px;
        color: #18ce0f;
        font-size: 11px;
    }
    .alert{
        padding-left: 0;
        padding-right: 0;
    }
</style>
<div class="page-header full-height" filter-color="orange">
    <!-- <div class="page-header-image" style="background-image:url(assets/img/bg8.jpg)"></div> -->
    <div class="content-center">
        <div class="container">
            <div class="col-md-4 content-center">
                <div class="card card-login card-plain">
                    <form class="form" method="POST" action="">
                        <div class="card-header text-center">
                            <div class="logo-container">
                                <img src="assets/img/now-logo.png" alt="">
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="alert" role="alert">
                                <?= $msg ?>
                            </div>
                            <input type="hidden" name="access-token" value="<?= $token; ?>">
                            <div class="input-group form-group-no-border input-lg">
                                <span class="input-group-addon">
                                    <i class="now-ui-icons ui-1_email-85"></i>
                                </span>
                                <input type="email" placeholder="taikhoancuatoi@gmail.com ..." class="form-control" required name="email"/>
                            </div>
                        </div>
                        <div class="card-footer text-center">
                            <button class="btn btn-primary btn-round btn-lg btn-block">
                                Email cho tôi
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="<?= get_assets_file('js/forgot-pass.js')?>"></script>