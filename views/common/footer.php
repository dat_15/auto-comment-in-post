    <footer class="footer ">
        <div class="container">
            <div class="copyright content-center text-center">
                &copy; Copyright
                <script>
                    document.write(new Date().getFullYear())
                </script> 
                Tự động bình luận. All rights reserved
            </div>
        </div>
    </footer>
</div>
</body>
<!--   Core JS Files   -->
<script src="<?= get_assets_file('js/core/jquery.3.2.1.min.js')?>" type="text/javascript"></script>
<script src="<?= get_assets_file('js/core/popper.min.js');?>" type="text/javascript"></script>
<script src="<?= get_assets_file('js/core/bootstrap.min.js');?>" type="text/javascript"></script>
<?php if( !isset($not_get_js) ):?>
<script src="<?= get_assets_file('js/auto-comment.js');?>" type="text/javascript"></script>
<?php endif;?>
</html>