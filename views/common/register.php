<style>
    @media only screen and (max-width:460px){
        .login-page .card-login .logo-container{
            margin-bottom:0;
            width:40px;
        }
    }
    .form-group-danger {
        background-color: rgba(255, 108, 0, 0.3);
        border-radius: 30px;
    }
    .input-group.form-group-success:after {
        font-family: 'Nucleo Outline';
        content: "\ea22";
        display: inline-block;
        position: absolute;
        right: 20px;
        top: 15px;
        color: #18ce0f;
        font-size: 11px;
    }
</style>
<div class="modal fade modal-mini" id="alert-model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header justify-content-center">
                <div class="modal-profile">
                    <i class="now-ui-icons ui-1_email-85"></i>
                </div>
            </div>
            <div class="modal-body">
                <p>Always have an access to your profile</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal">Đóng</button>
            </div>
        </div>
    </div>
</div>
<div class="page-header full-height" filter-color="orange">
    <div class="content-center">
        <div class="container">
            <div class="col-md-4 content-center">
                <div class="card card-login card-plain">
                    <form class="form" method="POST" action="">
                        <div class="card-header text-center">
                            <div class="logo-container">
                                <img src="assets/img/logo.png" alt="">
                            </div>
                        </div>
                        <div class="card-body">
                            <input type="hidden" name="access-token" value="<?= $token; ?>">
                            <div class="row">
                                <div class="col-6 pr-1">
                                    <div class="form-group-no-border input-group input-lg mb-10">
                                        <span class="input-group-addon">
                                            <i class="now-ui-icons users_circle-08"></i>
                                        </span>
                                        <input type="text" name="first_name" class="form-control text-capitalize" 
                                            placeholder="Họ " pattern="<?= NAME_PATTERN; ?>" required 
                                            title="Họ chỉ chứa chữ, Dài từ 2 - 25 ký tự">
                                    </div>
                                </div>
                                <div class="col-6 pl-1">
                                    <div class="form-group-no-border input-group input-lg mb-10">
                                        <span class="input-group-addon">
                                            <i class="now-ui-icons text_caps-small"></i>
                                        </span>
                                        <input type="text" name="last_name" class="form-control text-capitalize" 
                                            placeholder="Tên " pattern="<?= NAME_PATTERN; ?>" required 
                                            title="Tên chỉ chứa chữ, Dài từ 2 - 25 ký tự">
                                    </div>
                                </div>
                            </div>
                            <div class="input-group form-group-no-border input-lg">
                                <span class="input-group-addon">
                                    <i class="now-ui-icons users_single-02"></i>
                                </span>
                                <input type="text" name="username" class="form-control" 
                                    pattern="<?= USERNAME_PATTERN ?>" placeholder="Tên đăng nhập ..." required 
                                    title="Tên đăng nhập chỉ chứa chữ, số và gạch dưới '_'. Dài từ 8 - 30 ký tự">
                            </div>
                            <div class="input-group form-group-no-border input-lg">
                                <span class="input-group-addon">
                                    <i class="now-ui-icons ui-1_email-85"></i>
                                </span>
                                <input type="email" name="email" class="form-control" placeholder="Email ..." required>
                            </div>
                            <div class="input-group form-group-no-border input-lg">
                                <span class="input-group-addon">
                                    <i class="now-ui-icons ui-1_lock-circle-open"></i>
                                </span>
                                <input type="password" name="password" placeholder="Mật khẩu ..." 
                                    pattern="<?= PASSWORD_PATTERN ?>" 
                                    class="form-control" required 
                                    title="Mật khẩu phải có 1 chữ hoa, 1 chữ thường, 1 số và 1 trong các ký tự (!@#_). Dài từ 8 - 30 ký tự"/>
                            </div>
                            <div class="input-group form-group-no-border input-lg mb-10">
                                <span class="input-group-addon">
                                    <i class="now-ui-icons ui-1_lock-circle-open"></i>
                                </span>
                                <input type="password" name="re-password" placeholder="Nhập lại ..." 
                                    pattern="<?= PASSWORD_PATTERN ?>" 
                                    class="form-control" required 
                                    title="Mật khẩu phải có 1 chữ hoa, 1 chữ thường, 1 số và 1 trong các ký tự (!@#_). Dài từ 8 - 30 ký tự"/>
                            </div>
                        </div>
                        <div class="card-footer text-center">
                            <button class="btn btn-primary btn-round btn-lg btn-block">
                                Bắt đầu
                            </button>
                        </div>
                        <div class="pull-left">
                            <h6>
                                <a href="<?= USER_LOGIN; ?>" class="link footer-link">Đã có tài khoản</a>
                            </h6>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<script src="<?= get_assets_file('js/register.js'); ?>"></script>
