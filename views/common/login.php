<style>
    @media only screen and (max-width:460px){
        .login-page .card-login .logo-container{
            margin-bottom:0;
            width:40px;
        }
    }
    .form-group-danger {
        background-color: rgba(255, 108, 0, 0.3);
        border-radius: 30px;
    }
    .input-group.form-group-success:after {
        font-family: 'Nucleo Outline';
        content: "\ea22";
        display: inline-block;
        position: absolute;
        right: 20px;
        top: 15px;
        color: #18ce0f;
        font-size: 11px;
    }
</style>
<div class="modal fade modal-mini" id="alert-model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header justify-content-center">
                <div class="modal-profile">
                    <i class="now-ui-icons ui-1_email-85"></i>
                </div>
            </div>
            <div class="modal-body">
                <p>Always have an access to your profile</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal">Đóng</button>
            </div>
        </div>
    </div>
</div>
<div class="page-header full-height" filter-color="orange">
    <div class="content-center">
        <div class="container">
            <div class="col-md-4 content-center">
                <div class="card card-login card-plain">
                    <form class="form" method="POST" action="">
                        <div class="card-header text-center">
                            <div class="logo-container">
                                <img src="assets/img/logo.png" alt="">
                            </div>
                        </div>
                        <div class="card-body">
                            <?php if( !empty($msg) ): ?>
                                <div class="alert" role="alert">
                                    <?= $msg ?>
                                </div>
                            <?php endif; ?>
                            <input type="hidden" name="access-token" value="<?= $token; ?>">
                            <div class="input-group form-group-no-border input-lg">
                                <span class="input-group-addon">
                                    <i class="now-ui-icons users_single-02"></i>
                                </span>
                                <input type="text" class="form-control" name="username" required pattern="<?= USERNAME_PATTERN ?>" placeholder="Tên đăng nhập ...">
                            </div>
                            <div class="input-group form-group-no-border input-lg">
                                <span class="input-group-addon">
                                    <i class="now-ui-icons ui-1_lock-circle-open"></i>
                                </span>
                                <input type="password" placeholder="Mật khẩu ..." class="form-control" required name="password" pattern="<?= PASSWORD_PATTERN ?>" />
                            </div>
                        </div>
                        <div class="card-footer text-center">
                            <button class="btn btn-primary btn-round btn-lg btn-block">
                                Bắt đầu
                            </button>
                        </div>
                        <div class="pull-left">
                            <h6>
                                <a href="<?= USER_REGISTER; ?>" class="link footer-link">Tạo tài khoản mới</a>
                            </h6>
                        </div>
                        <div class="pull-right">
                            <h6>
                                <a href="<?= FORGOT_PASSWORD; ?>" class="link footer-link">Quên mật khẩu?</a>
                            </h6>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<script src="<?= get_assets_file('js/login.js')?>"></script>