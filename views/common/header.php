
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/logo.png">
    <link rel="icon" type="image/png" href="assets/img/logo.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title><?= isset($title) ? $title : 'Trả lời tự động'; ?></title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <base href="<?= base_url; ?>">
    <link href="<?= get_assets_file('css/bootstrap.min.css'); ?>" rel="stylesheet" />
    <link href="<?= get_assets_file('css/auto-comment.css'); ?>" rel="stylesheet" />
    <script type="text/javascript" charset="utf-8" async defer>
        var API_URL = '<?= base_url; ?>api/';
        var DOMAIN = '<?= base_url; ?>';
    </script>
</head>

<body class="<?= $page ?>-page">
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg bg-white fixed-top navbar-transparent" color-on-scroll="500">
        <div class="container">
            <div class="dropdown button-dropdown">
                <a href="<?= base_url ?>" class="dropdown-toggle" id="navbarDropdown" data-toggle="dropdown">
                    <span class="button-bar"></span>
                    <span class="button-bar"></span>
                    <span class="button-bar"></span>
                </a>
            </div>
            <div class="navbar-translate">
                <a class="navbar-brand" href="<?= base_url?>">
                    Comment Now
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" data-nav-image="assets/img/blurred-image-1.jpg" data-color="orange">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="">
                            Trang chủ
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="dieu-khoan-su-dung">
                            Điều khoản sử dụng
                        </a>
                    </li>               
                    <!-- <li class="nav-item">
                        <a class="nav-link btn btn-primary" href="<?= USER_REGISTER; ?>">
                            <p>Đăng ký ngay</p>
                        </a>
                    </li> -->
                    <li class="nav-item">
                        <a class="nav-link btn btn-primary" href="<?= USER_LOGIN; ?>">
                            <p>Đăng nhập</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="wrapper">
