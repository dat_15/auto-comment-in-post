<div class="page-header page-header-small">
    <div class="page-header-image" data-parallax="true" style="background-image: url('assets/img/bg26.jpg');">
    </div>
    <div class="content-center">
        <div class="container">
            <div class="col-md-8 ml-auto mr-auto text-center">
                <h2 class="title">Điều khoản sử dụng</h2>
                <p class="description">
                    Rất mong bạn đọc và tìm hiểu những điều khoản giữa bạn và <strong><b>
                    Tự động bình luận</strong> để tránh ảnh hưởng đến hoạt động kinh doanh online của bạn.
                </p>
            </div>
        </div>
    </div>
</div>
<div class="section section-about-us">
    <div class="container">
        <div class="row">
            <div class="col-md-8 ml-auto mr-auto text-center">
                <h3 class="title">
                    Chào mừng bạn đến với dịch vụ <strong>Tự động bình luận</strong>
                </h3>
            </div>
        </div>
        <div class="section-story-overview">
            <h3>
                Bản quyền
            </h3>
            <p class="description">
                <strong>
                    Tuyên bố các quyền Sở hữu trí tuệ của <b>Tự động bình luận</b> đối với các đối tượng thể hiện trên website
                </strong>
            </p>
            <p>
                Trang web này và tất cả nội dung của website, bao gồm nhưng không giới hạn các văn bản, thiết kế, đồ họa, giao diện, hình ảnh, mã code đều thuộc bản quyền của <b>Tự động bình luận</b> hoặc bên thứ ba cấp phép cho <b>Tự động bình luận</b>. Bản quyền của <b>Tự động bình luận</b> được thể hiện trên website bằng dòng chữ tiếng Anh '© 2018 Copyright by <b>Tự động bình luận</b>. All rights reserved.'.
            </p>
            <h3>Nội dung</h3>
            <p class="description">
                Quyền và trách nhiệm của <b>Tự động bình luận</b>
            </p>
            <p>
                Khi sử dụng website, thông tin sẽ được truyền qua một phương tiện/thiết bị nằm ngoài sự kiểm soát của <b>Tự động bình luận</b>. Theo đó, <b>Tự động bình luận</b> không chịu trách nhiệm cho hoặc liên quan đến bất kỳ sự chậm trễ, thất bại, bị gián đoạn của bất kỳ dữ liệu hoặc các thông tin khác được truyền trong kết nối với việc sử dụng của website.
            </p>
            <p>
                Thông tin cá nhân như địa chỉ, email, số điện thoại của người sử dụng website (do người sử dụng tự nguyện cung cấp bằng các hình thức khác nhau trên website) có thể được sử dụng nội bộ cho mục đích nâng cấp các sản phẩm, dịch vụ của <b>Tự động bình luận</b>. <b>Tự động bình luận</b> cam kết không chủ động tiết lộ bất kỳ thông tin nào của người sử dụng cho bên thứ ba, ngoại trừ trường hợp có yêu cầu bằng văn bản của các đơn vị điều tra theo đúng luật pháp hiện hành. Khi đó, <b>Tự động bình luận</b> sẽ thông báo tới người sử dụng bằng văn bản. Tuy nhiên, mặc dù có các công nghệ bảo mật và hệ thống cũng được trang bị rất nhiều tính năng bảo mật, nhưng không một dữ liệu nào được truyền trên đường truyền internet mà có thể được bảo mật 100%. Do vậy, <b>Tự động bình luận</b> không thể đưa ra một cam kết chắc chắn rằng thông tin bạn cung cấp cho chúng tôi sẽ được bảo mật một cách tuyệt đối, và cũng sẽ không chịu trách nhiệm trong trường hợp có sự truy cập trái phép thông tin cá nhân của người sử dụng. Tất cả các thông tin, tài liệu không mang tính chất riêng tư được người sử dụng đưa lên website đều không được xem là thông tin mật, <b>Tự động bình luận</b> được phép toàn quyền sử dụng hay chuyển tải cho bất kỳ mục đích nào.
            </p>
            <p>
                Bạn phải ‎ý thức và bảo đảm những thông tin, tài liệu mà bạn gửi phải nằm trong quyền hạn sử dụng của bạn; điều đó có nghĩa <b>Tự động bình luận</b> sẽ không vi phạm bất cứ quyền lợi nào của bên thứ ba.
            </p>
            <h3>Quyền và trách nhiệm của người sử dụng</h3>
            <p>
                <b>Tự động bình luận</b> cho phép người sử dụng xem, chiết xuất thông tin trên website (in, tải, chuyển tiếp…) hoặc chia sẻ cho người khác nhưng chỉ cho mục đích sử dụng cá nhân và phi thương mại với điều kiện phải trích dẫn thông báo bản quyền sau đây: '©2018 Copyright by <b>Tự động bình luận</b>.”
            </p>
            <p>
                Bạn đảm bảo tuân theo pháp luật và các quy định liên quan đến việc sử dụng website của <b>Tự động bình luận</b>; Không can thiệp, gây ảnh hưởng đến việc sử dụng website của những người sử dụng khác; Không can thiệp vào hoạt động và quản lý website của <b>Tự động bình luận</b>.
            </p>
            <p>

                Bạn nhận thức rõ và chấp nhận rằng <b>Tự động bình luận</b> và/hoặc các công ty thành viên/đơn vị trực thuộc /nhân viên <b>Tự động bình luận</b> không chịu trách nhiệm đối với bất kỳ tổn thất, thiệt hại, chi phí phát sinh từ bất kỳ quyết định nào của bạn khi sử dụng bất kỳ thông tin nào trên website với bất kỳ nguyên nhân gì.
            </p>
            <p>
                Nếu bạn không bằng lòng với bất kỳ thông tin nào trên Website hoặc với bất kỳ điều khoản và điều kiện sử dụng thông tin trên Website này thì phương thức duy nhất bạn nên thực hiện là chấm dứt truy cập/sử dụng thông tin trên website.
            </p>
            <h3>ĐIỀU KHOẢN CHUNG</h3>
            <p>Điều khoản sử dụng được điều chỉnh bởi pháp luật Việt Nam.</p>
            <p>Một điều khoản sử dụng bị vô hiệu theo quyết định của tòa án có thẩm quyền sẽ không ảnh hưởng đến tính hiệu lực của các điều khoản còn lại.</p>
            <p>Thông báo này và tất cả các điều khoản sử dụng tạo thành toàn bộ thỏa thuận giữa <b>Tự động bình luận</b> và bạn liên quan đến việc sử dụng các thông tin trên website. <b>Tự động bình luận</b> có thể điều chỉnh nội dung thông báo này bất cứ lúc nào bằng cách cập nhật lên website. Bạn nên thường xuyên truy cập website để theo dõi các quy định ràng buộc này khi sử dụng.</p>
            <h3>Thông tin liên hệ</h3>
            <p>
                <i class="now-ui-icons location_pin"></i> Ngõ 10 Láng Hạ - Đống Đa - Hà Nội
            </p>
            <p>
                <i class="now-ui-icons users_single-02"></i> 0166.2727.846
            </p>
        </div>
    </div>
</div>
