
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/logo.png">
    <link rel="icon" type="image/png" href="assets/img/logo.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title><?= isset($title) ? $title : 'Trả lời tự động'; ?></title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <base href="<?= base_url; ?>">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <link href="<?= get_assets_file('css/admin/bootstrap.min.css'); ?>" rel="stylesheet" />
    <link href="<?= get_assets_file('css/dashboard.min.css'); ?>" rel="stylesheet" />
    <link href="<?= get_assets_file('demo/demo.css'); ?>" rel="stylesheet" />
    <script type="text/javascript" charset="utf-8" async defer>
        var API_URL = '<?= base_url; ?>api/';
        var DOMAIN = '<?= base_url; ?>';
    </script>
</head>

<body class="sidebar-mini">

    <div class="wrapper ">
        <div class="sidebar" data-color="orange">
            <div class="logo">
                <a href="<?= base_url ?>" class="simple-text logo-mini">
                    BL
                </a>
                <a href="<?= base_url ?>" class="simple-text logo-normal">
                    Bình luận
                </a>
                <div class="navbar-minimize">
                    <button id="minimizeSidebar" class="btn btn-simple btn-icon btn-neutral btn-round">
                        <i class="now-ui-icons text_align-center visible-on-sidebar-regular"></i>
                        <i class="now-ui-icons design_bullet-list-67 visible-on-sidebar-mini"></i>
                    </button>
                </div>
            </div>
            <div class="sidebar-wrapper" style="overflow: hidden">
                <div class="user">
                    <div class="photo">
                        <img src="assets/img/james.jpg" />
                    </div>
                    <div class="info">
                        <a data-toggle="collapse" href="#collapseExample" class="collapsed">
                            <span class="text-uppercase">
                                <?php echo $user['last_name'] ?>
                                <b class="caret"></b>
                            </span>
                        </a>
                        <div class="clearfix"></div>
                        <div class="collapse" id="collapseExample">
                            <ul class="nav">
                                <li>
                                    <a href="<?= base_url ?>manage/profile">
                                        <span class="sidebar-mini-icon">HS</span>
                                        <span class="sidebar-normal">Hồ sơ</span>
                                  </a>
                                </li>
                                <li>
                                    <a href="<?= LOGOUT; ?>">
                                        <span class="sidebar-mini-icon">T</span>
                                        <span class="sidebar-normal">Đăng xuất</span>
                                  </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <ul class="nav">
                    <li >
                        <a href="<?= base_url ?>manage/main">
                            <i class="now-ui-icons education_paper"></i>
                            <p>Liên hết facebook</p>
                        </a>
                    </li>
                    <li >
                        <a href="<?= base_url ?>manage/content">
                            <i class="now-ui-icons ui-1_calendar-60"></i>
                            <p>Cài đặt nội dung</p>
                        </a>
                    </li>
                    <li >
                        <a href="<?= base_url ?>manage/verify-token">
                            <i class="now-ui-icons objects_planet"></i>
                            <p>Xác nhận lại</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="main-panel">
            <nav class="navbar navbar-expand-lg navbar-transparent  navbar-absolute bg-primary fixed-top">
                <div class="container-fluid">
                    <div class="navbar-wrapper">
                        <div class="navbar-toggle">
                            <button type="button" class="navbar-toggler">
                                <span class="navbar-toggler-bar bar1"></span>
                                <span class="navbar-toggler-bar bar2"></span>
                                <span class="navbar-toggler-bar bar3"></span>
                            </button>
                        </div>
                        <a class="navbar-brand" href="#pablo"><?= $title ?></a>
                    </div>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-bar navbar-kebab"></span>
                        <span class="navbar-toggler-bar navbar-kebab"></span>
                        <span class="navbar-toggler-bar navbar-kebab"></span>
                    </button>
                    <div class="collapse navbar-collapse justify-content-end" id="navigation">
                        <!-- <form>
                            <div class="input-group no-border">
                                <input type="text" value="" class="form-control" placeholder="Tìm kiếm...">
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <i class="now-ui-icons ui-1_zoom-bold"></i>
                                    </div>
                                </div>
                            </div>
                        </form> -->
                    </div>
                </div>
            </nav>


