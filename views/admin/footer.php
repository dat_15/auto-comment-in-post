            <footer class="footer ">
                <div class="container">
                    <div class="copyright content-center text-center">
                        &copy; Copyright
                        <script>
                            document.write(new Date().getFullYear())
                        </script> 
                        Nguyễn Tiến Đạt
                    </div>
                </div>
            </footer>
        </div>
    </div>
</body>
<!--   Core JS Files   -->
<script src="<?= get_assets_file('js/core/jquery.min.js')?>" type="text/javascript"></script>
<script src="<?= get_assets_file('js/core/popper.min.js');?>" type="text/javascript"></script>
<script src="<?= get_assets_file('js/core/bootstrap.min.js');?>" type="text/javascript"></script>
<script src="<?= get_assets_file('js/plugins/perfect-scrollbar.jquery.min.js');?>" type="text/javascript"></script>
<script src="<?= get_assets_file('js/plugins/bootstrap-switch.js');?>" type="text/javascript"></script>
<script src="<?= get_assets_file('js/plugins/bootstrap-notify.js');?>" type="text/javascript"></script>
<script src="<?= get_assets_file('js/dashboard.min.js');?>" type="text/javascript"></script>
</html>