<link rel="stylesheet" type="text/css" href="<?= get_assets_file('css/admin/set-content.css');?>">
<div class="panel-header">
   <div class="header text-center">
        <h2 class="title">{{title}}</h2>
        <p class="category" v-html="description"></p>
    </div>
</div>
<div class="content">
    <div class="row">
        <div class="col-md-12 ml-auto mr-auto text-center">
            <div class="card">
                <div class="card-body">
                    <div class="loader" v-if="show_loading">
                        <svg version="1.1" id="Layer_1" x="0px" y="0px"
                            width="43px" height="30px" viewBox="0 0 43 30" style="enable-background:new 0 0 50 50;" xml:space="preserve">
                            <rect x="0" y="13" width="4" height="5" fill="#333">
                                <animate attributeName="height" attributeType="XML"
                                    values="5;21;5" 
                                    begin="0s" dur="0.6s" repeatCount="indefinite" />
                                <animate attributeName="y" attributeType="XML"
                                    values="13; 5; 13"
                                    begin="0s" dur="0.6s" repeatCount="indefinite" />
                            </rect>
                            <rect x="10" y="13" width="4" height="5" fill="#333">
                                <animate attributeName="height" attributeType="XML"
                                    values="5;21;5" 
                                    begin="0.15s" dur="0.6s" repeatCount="indefinite" />
                                <animate attributeName="y" attributeType="XML"
                                    values="13; 5; 13"
                                    begin="0.15s" dur="0.6s" repeatCount="indefinite" />
                            </rect>
                            <rect x="20" y="13" width="4" height="5" fill="#333">
                                <animate attributeName="height" attributeType="XML"
                                    values="5;21;5" 
                                    begin="0.3s" dur="0.6s" repeatCount="indefinite" />
                                <animate attributeName="y" attributeType="XML"
                                    values="13; 5; 13"
                                    begin="0.3s" dur="0.6s" repeatCount="indefinite" />
                            </rect>
                        </svg>
                    </div>
                    <input type="hidden" name="access-token" value="<?= $token; ?>">
                    <div v-if="!show_loading && is_connected && subscribed_pages.length">
                        <button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#noticeModal">
                            <i class="now-ui-icons ui-1_simple-add"></i> Thêm mới bài post
                        </button>
                        <h3 class="text-center" v-if="list_posts.length">
                            Danh sách sản phẩm
                        </h3>
                        <h3 class="text-center text-muted" v-if="!list_posts.length">
                            Chưa có sản phẩm nào! Vui lòng thêm mới
                        </h3>
                        <div class="table-responsive" v-if="list_posts.length">
                            <table class="table">
                                <thead class="text-primary">
                                    <tr>
                                        <th class="text-center col-xs-3"><small>Post</small></th>
                                        <th class="text-center"><small>Nội dung tin nhắn</small></th>
                                        <th class="text-center"><small>Nội dung trả lời</small></th>
                                        <th class="text-center"><small>Trạng thái</small></th>
                                        <th class="text-center"><small>Hành động</small></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="p in list_posts" class="text-left">
                                        <td>
                                            <img v-bind:src="p.post_thumb" alt="">
                                            {{p.post_content}}
                                        </td>
                                        <td>
                                            <span v-if="!is_editting || cur_post_id !== p.post_id">
                                                {{p.comment_reply}}
                                            </span>
                                            <div class="form-group" v-if="is_editting && cur_post_id === p.post_id">
                                                <textarea v-model="p.comment_reply" class="form-control">
                                                    {{p.comment_reply}}
                                                </textarea>
                                            </div>
                                        </td>
                                        <td>
                                            <span v-if="!is_editting || cur_post_id !== p.post_id">
                                                {{p.message_reply}}
                                            </span>
                                            <div class="form-group" v-if="is_editting && cur_post_id === p.post_id">
                                                <textarea v-model="p.message_reply" class="form-control">
                                                    {{p.message_reply}}
                                                </textarea>
                                            </div>
                                        </td>
                                        <td>
                                            <span v-if="!is_editting || cur_post_id !== p.post_id">
                                                {{p.is_running == 1 ? 'Đang chạy' : 'Tạm dừng'}}
                                            </span>
                                            <div v-if="is_editting && cur_post_id === p.post_id">
                                                <p class="category">
                                                    {{p.is_running == 1 ? 'Đang chạy' : 'Tạm dừng'}}
                                                </p>
                                                <input 
                                                    v-model="p.is_running" 
                                                    type="checkbox" name="checkbox"
                                                    class="bootstrap-switch"
                                                    data-on-label="<i class='now-ui-icons ui-1_check'></i>"
                                                    data-off-label="<i class='now-ui-icons ui-1_simple-remove'></i>"
                                                />
                                            </div>
                                        </td>
                                        <td>
                                            <button 
                                                type="button" data-toggle="tooltip" 
                                                class="btn btn-success btn-icon btn-neutral" 
                                                data-original-title="Chỉnh sửa" title="Chỉnh sửa" 
                                                v-if="!is_editting || cur_post_id !== p.post_id" 
                                                v-on:click="show_editing(p)">
                                                <i class="now-ui-icons ui-2_settings-90"></i>
                                            </button>
                                            <button 
                                                type="button" data-toggle="tooltip" 
                                                class="btn btn-success btn-icon btn-neutral" 
                                                data-original-title="Chỉnh sửa" title="Chỉnh sửa" 
                                                v-if="is_editting && cur_post_id === p.post_id" 
                                                v-on:click="save_post(p)">
                                                <i class="now-ui-icons ui-1_check"></i>
                                            </button>
                                            <button 
                                                type="button" data-toggle="tooltip" 
                                                class="btn btn-danger btn-icon btn-neutral" 
                                                data-original-title="Xóa bài" title="Xóa bài"  
                                                v-if="!is_editting || cur_post_id !== p.post_id" 
                                                v-on:click="del_post(p.post_id)">
                                                <i class="now-ui-icons ui-1_simple-remove"></i>
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div v-if="(!show_loading && !is_connected) || (!show_loading && is_connected && !subscribed_pages.length)">
                        Vui lòng đăng ký fanpage <a href="manage/main">tại đây</a>!
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="noticeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-notice">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="now-ui-icons ui-1_simple-remove"></i>
                </button>
                <h5 class="modal-title" id="myModalLabel" v-html="model.title"></h5>
            </div>
            <div class="modal-body">
                <div class="loader text-center" v-if="model.show_loading">
                    <svg version="1.1" id="Layer_1" x="0px" y="0px"
                        width="43px" height="30px" viewBox="0 0 43 30" style="enable-background:new 0 0 50 50;" xml:space="preserve">
                        <rect x="0" y="13" width="4" height="5" fill="#333">
                            <animate attributeName="height" attributeType="XML"
                                values="5;21;5" 
                                begin="0s" dur="0.6s" repeatCount="indefinite" />
                            <animate attributeName="y" attributeType="XML"
                                values="13; 5; 13"
                                begin="0s" dur="0.6s" repeatCount="indefinite" />
                        </rect>
                        <rect x="10" y="13" width="4" height="5" fill="#333">
                            <animate attributeName="height" attributeType="XML"
                                values="5;21;5" 
                                begin="0.15s" dur="0.6s" repeatCount="indefinite" />
                            <animate attributeName="y" attributeType="XML"
                                values="13; 5; 13"
                                begin="0.15s" dur="0.6s" repeatCount="indefinite" />
                        </rect>
                        <rect x="20" y="13" width="4" height="5" fill="#333">
                            <animate attributeName="height" attributeType="XML"
                                values="5;21;5" 
                                begin="0.3s" dur="0.6s" repeatCount="indefinite" />
                            <animate attributeName="y" attributeType="XML"
                                values="13; 5; 13"
                                begin="0.3s" dur="0.6s" repeatCount="indefinite" />
                        </rect>
                    </svg>
                </div>
                <div class="instruction table-responsive" v-if="model.step == 1">
                    <table class="table">
                        <thead class="text-primary">
                            <tr>
                                <th></th>
                                <th>Page</th>
                                <th>Chọn</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="p in subscribed_pages">
                                <td>
                                    <img v-bind:src="'https://graph.facebook.com/v3.0/'+p.fanpage_id+'/picture?type=square'" alt="">
                                </td>
                                <td class="text-left">{{p.fanpage_name}}</td>
                                <td class="text-left">
                                    <button class="btn btn-primary btn-simple" 
                                        v-on:click="select_page(p.fanpage_id, $event)"
                                    >
                                        <i class="now-ui-icons arrows-1_minimal-right"></i>
                                    </button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="instruction" v-if="model.step == 2 && !model.show_loading ">
                    <ul class="nav nav-pills nav-pills-primary nav-pills-icons justify-content-center" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#link7" role="tablist">
                                <i class="now-ui-icons files_single-copy-04"></i>
                                Bài post
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#link8" role="tablist">
                                <i class="now-ui-icons design_image"></i>
                                Ảnh
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content tab-space tab-subcategories">
                        <div class="tab-pane active" id="link7">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead class="text-primary">
                                        <th>
                                        </th>
                                        <th class="text-center">Ảnh</th>
                                        <th class="text-center">Nội dung</th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="3">
                                                <div class="input-group">
                                                    <input type="text" @keyup="find_post($event)" class="form-control" placeholder="Tìm kiếm post">
                                                    <div class="input-group-append">
                                                        <div class="input-group-text">
                                                            <i class="now-ui-icons ui-1_zoom-bold"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr v-for="p in model.result_posts">
                                            <td  class="text-center" >
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" v-on:change="select_posts(p, $event)" v-bind:id="p.id" type="checkbox">
                                                        <span class="form-check-sign"></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <img v-bind:src="p.picture" alt="">
                                            </td>
                                            <td>
                                                {{p.message != undefined ? p.message.split(/\s+/).slice(0,10).join(" ") + ' ...' : p.story}}
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="link8">
                            <label v-for="p in model.photos" v-bind:class="{ active: is_selected_photo(p)}" v-on:click="select_photos(p, $event)">
                                <img v-bind:src="p.picture" alt="">
                            </label >
                        </div>
                    </div>
                    <div class="text-center">
                        <button class="btn btn-primary btn-simple" 
                            v-on:click="backToPreviosStep()"
                        >
                            <i class="now-ui-icons arrows-1_minimal-left"></i>
                        </button>
                        <button class="btn btn-primary btn-simple" 
                            v-on:click="goToNextStep()" 
                            v-if="model.selected_photos.length || model.selected_posts.length"
                        >
                            <i class="now-ui-icons arrows-1_minimal-right"></i>
                        </button>
                    </div>
                </div>
                <div class="instruction" v-if="model.step == 3 && !model.show_loading ">
                    <div class="form-group">
                        <span class="form-text">Nội dung trả lời bình luận</span>
                        <textarea v-model="model.comment_reply" class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                        <span class="form-text">Nội dung trả lời tin nhắn</span>
                        <textarea v-model="model.message_reply" class="form-control"></textarea>
                    </div>
                    <div class="text-center">
                        <button class="btn btn-primary btn-simple" 
                            v-on:click="backToPreviosStep()"
                        >
                            <i class="now-ui-icons arrows-1_minimal-left"></i>
                        </button>
                        <button class="btn btn-primary btn-simple" 
                            v-on:click="addPosts()"
                            v-if="model.comment_reply != '' && model.message_reply != ''"
                        >
                            Thêm mới nội dung
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?= get_assets_file('js/core/vue.js') ?>" type="text/javascript"></script>
<script src="<?= get_assets_file('js/admin/set-message.js');?>" type="text/javascript"></script>