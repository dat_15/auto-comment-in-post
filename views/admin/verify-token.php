<div class="panel-header">
   <div class="header text-center">
        <h2 class="title">Chào mừng bạn quay lại!</h2>
        <p class="category">Sau khi liên kết tài khoản từ 30 - 60 ngày thì token của bạn sẽ hết hạn.</p>
        <p class="category">Vui lòng xác nhận tài khoản với facebook để lấy token mới!</p>
    </div>
</div>
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-10 ml-auto mr-auto text-center">
                <div class="card">
                    <div class="card-body">
                        <div class="loader" v-if="show_loading">
                            <svg version="1.1" id="Layer_1" x="0px" y="0px"
                                width="43px" height="30px" viewBox="0 0 43 30" style="enable-background:new 0 0 50 50;" xml:space="preserve">
                                <rect x="0" y="13" width="4" height="5" fill="#333">
                                    <animate attributeName="height" attributeType="XML"
                                        values="5;21;5" 
                                        begin="0s" dur="0.6s" repeatCount="indefinite" />
                                    <animate attributeName="y" attributeType="XML"
                                        values="13; 5; 13"
                                        begin="0s" dur="0.6s" repeatCount="indefinite" />
                                </rect>
                                <rect x="10" y="13" width="4" height="5" fill="#333">
                                    <animate attributeName="height" attributeType="XML"
                                        values="5;21;5" 
                                        begin="0.15s" dur="0.6s" repeatCount="indefinite" />
                                    <animate attributeName="y" attributeType="XML"
                                        values="13; 5; 13"
                                        begin="0.15s" dur="0.6s" repeatCount="indefinite" />
                                </rect>
                                <rect x="20" y="13" width="4" height="5" fill="#333">
                                    <animate attributeName="height" attributeType="XML"
                                        values="5;21;5" 
                                        begin="0.3s" dur="0.6s" repeatCount="indefinite" />
                                    <animate attributeName="y" attributeType="XML"
                                        values="13; 5; 13"
                                        begin="0.3s" dur="0.6s" repeatCount="indefinite" />
                                </rect>
                            </svg>
                        </div>
                        <input type="hidden" name="access-token" value="<?= $token; ?>">
                        <div v-if="!show_loading">
                            <button class="btn btn-lg btn-primary" id="connect-with-facebook-btn" v-on:click="login_fb()">
                                <img src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTYuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgd2lkdGg9IjE2cHgiIGhlaWdodD0iMTZweCIgdmlld0JveD0iMCAwIDk2LjEyNCA5Ni4xMjMiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDk2LjEyNCA5Ni4xMjM7IiB4bWw6c3BhY2U9InByZXNlcnZlIj4KPGc+Cgk8cGF0aCBkPSJNNzIuMDg5LDAuMDJMNTkuNjI0LDBDNDUuNjIsMCwzNi41Nyw5LjI4NSwzNi41NywyMy42NTZ2MTAuOTA3SDI0LjAzN2MtMS4wODMsMC0xLjk2LDAuODc4LTEuOTYsMS45NjF2MTUuODAzICAgYzAsMS4wODMsMC44NzgsMS45NiwxLjk2LDEuOTZoMTIuNTMzdjM5Ljg3NmMwLDEuMDgzLDAuODc3LDEuOTYsMS45NiwxLjk2aDE2LjM1MmMxLjA4MywwLDEuOTYtMC44NzgsMS45Ni0xLjk2VjU0LjI4N2gxNC42NTQgICBjMS4wODMsMCwxLjk2LTAuODc3LDEuOTYtMS45NmwwLjAwNi0xNS44MDNjMC0wLjUyLTAuMjA3LTEuMDE4LTAuNTc0LTEuMzg2Yy0wLjM2Ny0wLjM2OC0wLjg2Ny0wLjU3NS0xLjM4Ny0wLjU3NUg1Ni44NDJ2LTkuMjQ2ICAgYzAtNC40NDQsMS4wNTktNi43LDYuODQ4LTYuN2w4LjM5Ny0wLjAwM2MxLjA4MiwwLDEuOTU5LTAuODc4LDEuOTU5LTEuOTZWMS45OEM3NC4wNDYsMC44OTksNzMuMTcsMC4wMjIsNzIuMDg5LDAuMDJ6IiBmaWxsPSIjRkZGRkZGIi8+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPC9zdmc+Cg==" /> 
                                Xác nhận tài khoản
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?= get_assets_file('js/core/vue.js') ?>" type="text/javascript" charset="utf-8" async defer></script>
<script src="<?= get_assets_file('js/admin/verify-token.js');?>" type="text/javascript"></script>